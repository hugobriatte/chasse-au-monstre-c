#include "hunter.h"

hunter createHunter(boolean isAI){
  hunter res;
  res.isAI = isAI;
  res.nbStunt = 0;
  res.allSeen = emptyLinkedList;
  res.radius = 0;
  return res;
}

position hunterPlay(hunter * hunt, terrain ter, int turn){
  if(hunt->isAI){
    arrayList possibilities;
    position selectedPos;
    cell * selectedCell;
    if(linkedListIsEmpty(hunt->allSeen)){
      arrayList allWalkable = getAllPosWalkableTer(ter);
      collectionEntry * entry;
      possibilities = arrayListCreateWithSize(arrayListLength(allWalkable));
      FOREACHARRAYLIST(entry, allWalkable){
	arrayListAddObj(&possibilities, entry->pointer, entry->size);
      }
      arrayListDestroy(&allWalkable);
    }else{
      /*TODO faire un getAllAccesibleAndRemove pour le Hunter */
      possibilities = getAllAccesibleAndRemoveHunter(ter, *((position *) linkedListGetIdx(hunt->allSeen, 0).pointer), turn - hunt->radius, hunt->allSeen);
    }
    selectedPos = *((position *) arrayListGetRandom(possibilities).pointer);
    selectedCell = getCellPos(ter, selectedPos);
    if(hasBeenVisited(*selectedCell)){
      hunt->radius = turn - selectedCell->turn;
      linkedListAddObj(&(hunt->allSeen), &selectedPos, sizeof(position));
    }
    arrayListDestroy(&possibilities);
    return selectedPos;
  }
  return createPosition(50, 50);
}

void hunterDestroy(hunter hunt){
  linkedListDestroy(hunt.allSeen);
}
