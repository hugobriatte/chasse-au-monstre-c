/**
 * \file position.c
 * \brief Structure position et ses fonctions
 */

#include "position.h"

position createPosition(int x, int y){
  position res;
  res.x = x;
  res.y = y;
  return res;
}

position getAddPos(position posA, position posB) {
  return getAddXY(posA, posB.x, posB.y);
}

position getAddXY(position pos, int x, int y) {
  position res;
  res.x = pos.x + x;
  res.y = pos.y + y;
  return res;
}

position getAddDir(position pos, direction dir){
  return getAddPos(pos, getPosOf(dir));
}

position getPosOf(direction d){
  position res;
  res.x = getDirX(d);
  res.y = getDirY(d);
  return res;
}

boolean positionEquals(position d1, position d2){
  if(&d1 == &d2) return true;
  else if(d1.x == d2.x && d1.y == d2.y) return true;
  else return false;
}

void movePosToDir(position * pos, direction direction){
  translateWithPos(pos, getPosOf(direction));
}

void translateWithPos(position * pos, position vector){
  translateWithInt(pos, vector.x, vector.y);
}

void translateWithInt(position * pos, int x, int y){
  *pos = getAddXY(*pos, x, y);
}
