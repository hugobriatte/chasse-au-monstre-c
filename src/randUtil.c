#include "randUtil.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "boolean.h"

int randMinMax(int min, int max){
  static boolean fait = false;
  if(!fait){
    /*unsigned int code = (unsigned int) time(NULL);
    printf("code : %d \n", code);
    srand(code);*/
    srand(1563870451);
    fait = true;
  }
  return min + (rand() / (RAND_MAX / (max - min)));
}
