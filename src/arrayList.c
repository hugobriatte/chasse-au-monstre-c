#include "arrayList.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * \file list.c
 * \brief Implémentation des fonctions de liste (list.h)
 */

/**
 * \fn void listSetNull(list list, int i)
 * \brief Set a null une case du tableau tout en le mettant a null
 * \param list La liste
 * \param i L'index de la case
 */
void arrayListSetNull(arrayList list, int i){
  if(arrayListIsValidIdx(list, i)){
    if(list.tab[i].pointer != NULL){
      free(list.tab[i].pointer);
      list.tab[i] = emptyCollectionEntry;
    }
  }
}

/**
 * \fn void listResize(list * list)
 * \brief Actualise la taille maximale en l'augmentant de 50%
 * \param list Le pointeur sur la liste
 */
void arrayListResize(arrayList * list){
  int i;
  list->maxSize *= 2;
  list->maxSize += 1;
  list->tab = (collectionEntry *) (realloc(list->tab, list->maxSize * sizeof(collectionEntry)));
  for(i = arrayListLength(*list); i < list->maxSize; i++){
    list->tab[i] = emptyCollectionEntry;
  }
}

arrayList arrayListCreateWithSize(int defSize){
  arrayList res;
  int i;
  res.maxSize = defSize;
  res.size = 0;
  res.tab = (collectionEntry *) (calloc(defSize, sizeof(collectionEntry)));
  for(i = 0; i < arrayListLength(res); i++){
    res.tab[i] = emptyCollectionEntry;
  }
  return res;
}

arrayList arrayListCreateWithDefault(void * toCopy, int sizeOf, int nbItems){
  int i;
  arrayList res = arrayListCreateWithSize(nbItems);
  for(i = 0; i < nbItems; i++){
    arrayListAddObj(&res, toCopy, sizeOf);
  }
  return res;
}

boolean arrayListIsEmpty(arrayList list){
  if(arrayListLength(list) == 0) return true;
  return false;
}

int arrayListLength(arrayList list){
  return list.size;
}

boolean arrayListIsValidIdx(arrayList list, int i){
  if(i>=0 && i < arrayListLength(list)) return true;
  else return false;
}

collectionEntry arrayListGet(arrayList list, int i){
  if(!arrayListIsValidIdx(list, i)) return emptyCollectionEntry;
  else return list.tab[i];
}

void arrayListSetObj(arrayList list, int i, void * object, int sizeOf){
  if(arrayListIsValidIdx(list, i)){
    arrayListSetNull(list, i);
    list.tab[i].pointer = memcpy(malloc(sizeOf), object, sizeOf);
    list.tab[i].size = sizeOf;
  }
}

void arrayListSwitch(arrayList list, int i1, int i2){
  if(arrayListIsValidIdx(list, i1) && arrayListIsValidIdx(list, i2)){
    collectionEntry tmp = list.tab[i1];
    list.tab[i1] = list.tab[i2];
    list.tab[i2] = tmp;
  }
}

void arrayListAddObj(arrayList * list, void * object, int sizeOf){
  arrayListAddObjIdx(list, list->size, object, sizeOf);
}

void arrayListAddObjIdx(arrayList * list, int i, void * object, int sizeOf){
  if(i >= 0 && i <= arrayListLength(*list)){
    int j;
    if(arrayListLength(*list) == list->maxSize) arrayListResize(list);
    list->size += 1;
    for(j = arrayListLength(*list) - 1; j > i; j--){
      arrayListSwitch(*list, j, j - 1);
    }
    arrayListSetObj(*list, i, object, sizeOf);
  }
}

int arrayListIndexOf(arrayList list, void * object, int sizeOf){
  int i = 0;
  while(i < arrayListLength(list)){
    if(list.tab[i].size == sizeOf && memcmp(list.tab[i].pointer, object, sizeOf) == 0) return i;
    i += 1;
  }
  return -1;
}

void arrayListRemoveIdx(arrayList * list, int idx){
  if(idx >= 0 && idx < arrayListLength(*list)){
    int i;
    for(i = idx; i < arrayListLength(*list); i++){
      arrayListSwitch(*list, i, i + 1);
    }
    list->size -= 1;
    free(list->tab[arrayListLength(*list)].pointer);
    list->tab[arrayListLength(*list)] = emptyCollectionEntry;
  }
}

void arrayListRemoveObj(arrayList * list, void * object, int sizeOf){
  arrayListRemoveIdx(list, arrayListIndexOf(*list, object, sizeOf));
}

void arrayListDestroy(arrayList * list){
  int i;
  for(i = 0; i < arrayListLength(*list); i++){
    free(list->tab[i].pointer);
  }
  free(list->tab);
}

int arrayListQuickSortPartition(arrayList list, int (*sortFunction)(void *, void *, void *), void * externalArg, int deb, int fin, int pivot){
  int i;
  int j;
  arrayListSwitch(list, pivot, fin);
  j = deb;
  for(i = deb; i < fin - 1; i++){
    if(sortFunction(arrayListGet(list, i).pointer, arrayListGet(list, fin).pointer, externalArg) <= 0){
      arrayListSwitch(list, i, j);
      j += 1;
    }
  }
  arrayListSwitch(list, fin, j);
  return j;
}

void arrayListQuickSort(arrayList list, int (*sortFunction)(void *, void *, void *), void * externalArg, int deb, int fin){
  if(deb < fin){
    int piv = abs(rand() - 1) / (RAND_MAX / (fin - deb + 1));
    piv = arrayListQuickSortPartition(list, sortFunction, externalArg, deb, fin, piv);
    arrayListQuickSort(list, sortFunction, externalArg, deb, piv - 1);
    arrayListQuickSort(list, sortFunction, externalArg, piv + 1, fin);
  }
}

void arrayListBubleSort(arrayList list, int (*sortFunction)(void *, void *, void *), void * externalArg){
  int i;
  for(i = arrayListLength(list) - 1; i >= 1; i--){
    int j;
    for(j = 0; j < i; j++){
      if(sortFunction(arrayListGet(list, i).pointer, arrayListGet(list, j).pointer, externalArg) <= 0){
	arrayListSwitch(list, i, j);
      }
    }
  }
}

void arrayListSort(arrayList list, int (*sortFunction)(void *, void *, void *), void * externalArg){
  arrayListBubleSort(list, sortFunction, externalArg);
  /*arrayListQuickSort(list, sortFunction, externalArg, 0, arrayListLength(list) - 1);*/
}
