/**
 * \file map.c
 * \brief Implémentation de la map et de ses fonctions
 */
#include "map.h"
#include <stdlib.h>

map mapCreate(){
  map res;
  res.keyList = NULL;
  res.valueList = arrayListCreate();
  return res;
}

boolean mapPlaced(map map, void * key, int sizeOf){
  if(linkedListIndexOf(map.keyList, key, sizeOf) == -1) return false;
  return true;
}

void mapPut(map * map, void * key, int sizeOfKey, void * value, int sizeOfValue){
  if(!mapPlaced(*map, key, sizeOfKey)) linkedListAddObj(&(map->keyList), key, sizeOfKey);
  arrayListAddObjIdx(&(map->valueList), linkedListIndexOf(map->keyList, key, sizeOfKey), value, sizeOfValue);
}

void mapRemove(map * map, void * key, int sizeOfKey){
  arrayListRemoveIdx(&(map->valueList), linkedListIndexOf(map->keyList, key, sizeOfKey));
  linkedListRemoveObj(&(map->keyList), key, sizeOfKey);
}

collectionEntry mapGet(map map, void * key, int sizeOfKey){
  return arrayListGet(map.valueList, linkedListIndexOf(map.keyList, key, sizeOfKey));
}

void mapDestroy(map * map){
  linkedListDestroy(map->keyList);
  arrayListDestroy(&(map->valueList));
}
