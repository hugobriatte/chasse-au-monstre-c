/**
 * \file direction.c
 * \brief Structure direction et ses fonctions
 * 
 * Une direction est une structure pouvant décrire les differents mouvements
 */
#include "direction.h"
#include <stdio.h>
#include <string.h>

int getDirX(direction d){
  return -1 + (d / 3);
}

int getDirY(direction d){
  return -1 + (d % 3);
}

/**
 * \fn static direction getDirOf(char * string)
 * \brief Retourne la direction correspondant a un string donnée
 * \param string Le string
 * \return La direction associé au string (NONE si incorrecte)
 */
static direction getDirOf(char * string){
  static const char * possibles[] = {"L-U", "L", "L-D", "U", "N", "D", "R-U", "R", "R-D"};
  int i = 0;
  while(i < 9 && strcmp(string, possibles[i]) != 0) i++;
  if(i == 10) return NONE;
  else return (direction) i;
}

direction readDirectionOf(FILE * file){
  char read[4];
  fscanf(file, "%s", read);
  return getDirOf(read);
}
