#include "monster.h"

monster createMonsterXY(int x, int y, boolean isAI){
  position pos = createPosition(x, y);
  return createMonsterPos(pos, isAI);
}

monster createMonsterPos(position pos, boolean isAI){
  monster res;
  res.pos = pos;
  res.isFree = true;
  res.isAI = isAI;
  return res;
}
