/**
 * \file cell.c
 * \brief Implémentation des fonctions sur les cellules
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cell.h"
#include "randUtil.h"

cellType getNextCellType(boolean isThereWall, boolean isThereViewPoint, boolean isThereTp){
  char wallCeiling, viewPointCeiling, tpCeiling;
  int res;
  
  if(isThereWall) wallCeiling = 2;
  else wallCeiling = 0;
  if(isThereViewPoint) viewPointCeiling = wallCeiling + 2;
  else viewPointCeiling = wallCeiling;
  if(isThereTp) tpCeiling = viewPointCeiling + 1;
  else tpCeiling = viewPointCeiling;
  
  res = randMax(10);

  if(res < wallCeiling) return WALL;
  else if(res < viewPointCeiling) return VIEW_POINT_CELL;
  else if(res < tpCeiling) return TP_CELL;
  else return WALKABLE_CELL;
}

/**
 * \fn char cellCharHidden(cellType type)
 * \brief Fonction qui retourne le caractère correspondant au type de la cellule en caché
 * \param type Le type de la cellule
 * \return Le caractère correspondant au type de la cellule en caché
 */
char cellCharHidden(cellType type) {
  if(type == WALL) return '#';
  else return ' ';
}

/**
 * \fn char cellCharComplete(cellType type)
 * \brief Fonction qui retourne le caractère correspondant au type de la cellule
 * \param type Le type de la cellule
 * \return Le caractère correspondant au type de la cellule
 */
char cellCharComplete(cellType type){
  if(type == WALL) return '#';
  else if(type == VIEW_POINT_CELL) return '^';
  else if(type == STUNT_TRAP) return '~';
  else if(type == TP_CELL) return '.';
  else return ' ';
}

/**
 * \fn char * getTurnNum(cell c, int nbChar)
 * \brief Fonction qui retourne un String correspondant au numero du tour donnée
 * \param c La cellule
 * \param nbChar Le nombre de caractère de la chaine
 * \return Ln String correspondant au numero du tour donnée
 */
char * getTurnNum(cell c, int nbChar){
  char * res = (char *) (calloc(nbChar + 1, sizeof(char)));
  int i;
  for(i = 0; i < nbChar; i++){
    sprintf(res, "%d", c.turn);
  }
  while(nbChar - strlen(res) > 0){
    strcat(res, " ");
  }
  res[nbChar] = '\0';
  return res;
}

cell createCellType(cellType type){
  return createCell(type, -1);
}

cell createCell(cellType type, int turn){
  cell res;
  res.typeCell = type;
  res.turn = turn;
  return res;
}

boolean hasBeenVisited(cell c){
  if(c.turn != -1) return true;
  return false;
}

char * cellCharForMonster(cell c, int nbChar){
  if(hasBeenVisited(c)){
    return getTurnNum(c, nbChar);
  }else {
    char * res = (char *) (calloc(nbChar + 1, sizeof(char)));
    memset(res, cellCharHidden(c.typeCell), nbChar);
    res[nbChar] = '\0';
    return res;
  }
}

char * cellCharForMonsterComplete(cell c, int nbChar){
  if(hasBeenVisited(c)) return getTurnNum(c, nbChar);
  else {
    char * res = (char *) (calloc(nbChar + 1, sizeof(char)));
    memset(res, cellCharComplete(c.typeCell), nbChar);
    res[nbChar] = '\0';
    return res;
  }
}

char * cellCharForHunter(cell c, int nbChar) {
  char * res = (char *) (calloc(nbChar + 1, sizeof(char)));
  memset(res, cellCharComplete(c.typeCell), nbChar);
  res[nbChar] = '\0';
  return res;
}
