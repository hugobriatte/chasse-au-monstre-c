#include "terrain.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "randUtil.h"
#include "maths.h"
 
static monster createMonsterTer(terrain ter, boolean isAI){
  int x = randMax(ter.nbCellX);
  int y = randMax(ter.nbCellY);
  getCellXY(ter, x, y)->typeCell = WALKABLE_CELL;
  return createMonsterXY(x, y, isAI);
}

terrain terrainCreateWith(int nbCellX, int nbCellY, boolean isThereWall, boolean isThereViewPoint, boolean isThereTpCell, boolean monsterAI){
  cell ** pointToPoint = (cell **) (calloc(nbCellY, sizeof(cell *)));
  int i;
  terrain res;
  for(i = 0; i < nbCellY; i++){
    int j;
    pointToPoint[i] = (cell *)(calloc(nbCellX, sizeof(cell)));
    for(j = 0; j < nbCellX; j++){
      pointToPoint[i][j] = createCellType(getNextCellType(isThereWall, isThereViewPoint, isThereTpCell));
    }
  }
  
  res.nbCellX = nbCellX;
  res.nbCellY = nbCellY;
  res.cells = pointToPoint;
  res.monstre = createMonsterTer(res, monsterAI);
  pointToPoint[res.monstre.pos.y][res.monstre.pos.x].turn = 1;
  
  for(i = 0; i < nbCellY; i++){
    int j;
    for(j = 0; j < nbCellX; j++){
      if(getCellXY(res, j, i)->typeCell == WALL){
	map allNear = getAllNearXY(res, j, i);
	map allWalkable = getAllNearWalkableXY(res, j, i);
	direction ** entry;
	FOREACHKEY(entry, allNear){
	  direction dir = **entry;
	  if(!mapPlaced(allWalkable, &dir, sizeof(direction)) && dir != NONE){
	    getCellPos(res, *((position *) mapGet(allNear, &dir, sizeof(direction)).pointer))->typeCell = WALKABLE_CELL;
	  }
	}
	mapDestroy(&allNear);
	mapDestroy(&allWalkable);
      }
    }
  }
  return res;
}

cell * getCellXY(terrain ter, int x, int y){
  if(isXYValid(ter, x, y)){
    return ter.cells[y] + x;
  }else{
    return NULL;
  }
}

cell * getCellPos(terrain ter, position pos){
  return getCellXY(ter, pos.x, pos.y);
}

void placePiegeXY(terrain ter, int x, int y){
  cell toPlace = createCell(STUNT_TRAP, -1);
  ter.cells[y][x] = toPlace;
}

void placePiegePos(terrain ter, position pos){
  placePiegeXY(ter, pos.x, pos.y);
}

boolean isCompletelyVisited(terrain ter){
  int x = 0, y = 0;
  while(y < ter.nbCellY){
    if(!hasBeenVisited(*getCellXY(ter, x, y))) return false;
    x += 1;
    if(x >= ter.nbCellX){
      y += 1;
      x = 0;
    }
  }
  return true;
}

boolean isXYValid(terrain ter, int x, int y){
  if(x >= 0 && x < ter.nbCellX && y >= 0 && y < ter.nbCellY) return true;
  else return false;
}

boolean isPosValid(terrain ter, position pos){
  return isXYValid(ter, pos.x, pos.y);
}

map getAllNearPos(terrain ter, position pos){
  map res = mapCreate();
  direction dir;
  for(dir = 0; dir <= RIGHT_DOWN; dir++){
    if(dir != NONE
       && isPosValid(ter, getAddDir(pos, dir))){
      position posAtDir = getAddDir(pos, dir);
      mapPut(&res, &dir, sizeof(direction), &posAtDir, sizeof(position));
    }
  }
  return res;
}

map getAllNearXY(terrain ter, int x, int y){
  position pos = createPosition(x, y);
  return getAllNearPos(ter, pos);
}

map getAllNearWalkablePos(terrain ter, position pos){
  map res = getAllNearPos(ter, pos);
  direction dir;
  for(dir = 0; dir <= RIGHT_DOWN; dir++){
    if(mapPlaced(res, &dir, sizeof(direction))){
      cell cellAtDir = *getCellPos(ter, *((position *) mapGet(res, &dir, sizeof(dir)).pointer));
      if(cellAtDir.typeCell == WALL){
	mapRemove(&res, &dir, sizeof(direction));
      }
    }
  }
  return res;
}

map getAllNearWalkableXY(terrain ter, int x, int y){
  position pos = createPosition(x, y);
  return getAllNearWalkablePos(ter, pos);
}

map getAllNearWalkableNotSeenPos(terrain ter, position pos){
  map res = getAllNearWalkablePos(ter, pos);
  direction dir;
  for(dir = 0; dir <= RIGHT_DOWN; dir++){
    if(mapPlaced(res, &dir, sizeof(direction))){
      cell cellAtDir = *getCellPos(ter, *((position *) mapGet(res, &dir, sizeof(dir)).pointer));
      if(hasBeenVisited(cellAtDir)){
	mapRemove(&res, &dir, sizeof(direction));
      }
    }
  }
  return res;
}

map getAllNearWalkableNotSeenXY(terrain ter, int x, int y){
  position pos = createPosition(x, y);
  return getAllNearWalkableNotSeenPos(ter, pos);
}

void terrainDestroy(terrain ter){
  int y;
  for(y = 0; y < ter.nbCellY; y++){
    free(ter.cells[y]);
  }
  free(ter.cells);
}

static arrayList hiddenGetAllAccesibleAndRemove(terrain ter, position pos, int nbCell, linkedList toRemove, boolean (*condition) (cell )){
  arrayList res = arrayListCreate();
  /*
  if(nbCell == 0) return res;
  else {
    map allNear = getAllNearPos(ter, pos);
    position ** pointer;
    FOREACHVALUE(pointer, allNear){
      position pos = **pointer;
      cell cellAt = *getCellPos(ter, pos);
      if(condition(cellAt) && arrayListIndexOf(res, *pointer, sizeof(position)) == -1 && linkedListIndexOf(toRemove, *pointer, sizeof(position)) == -1){
	arrayListAddObj(&res, *pointer, sizeof(position));
      }
      if(condition(cellAt) && nbCell > 1){
	arrayList newToAdd = hiddenGetAllAccesibleAndRemove(ter, pos, nbCell - 1, toRemove, condition);
	position ** pointerD;
	FOREACHARRAYLIST(pointerD, newToAdd){
	  if(arrayListIndexOf(res, *pointerD, sizeof(position)) == -1 && linkedListIndexOf(toRemove, *pointer, sizeof(position)) == -1){
	    arrayListAddObj(&res, *pointerD, sizeof(position));
	  }
	}
	arrayListDestroy(&newToAdd);
      }
    }
    mapDestroy(&allNear);
  }
  */
  int x;
  for(x = -nbCell; x <= nbCell; x++){
    int y;
    for(y = -nbCell; y <= nbCell ;y++){
      if(x != 0 || y != 0){
	position toAdd = getAddXY(pos, x, y);
	if(isPosValid(ter, toAdd)){
	  cell cellAt = *getCellPos(ter, toAdd);
	  if(condition(cellAt)){
	    arrayListAddObj(&res, &toAdd, sizeof(position));
	  }
	}
      }
    }
  }
  collectionRemoveAll(arrayListAsCollection(&res), linkedListAsCollection(&toRemove));
  return res;
}

static boolean conditionForGetAccesibleHunter(cell c){
  return c.typeCell != WALL;
}

static boolean conditionForGetAccesibleMonster(cell c){
  return conditionForGetAccesibleHunter(c) && !hasBeenVisited(c);
}

arrayList getAllAccesibleAndRemoveHunter(terrain ter, position pos, int nbCell, linkedList toRemove){
  static boolean (*cond) (cell ) = &conditionForGetAccesibleHunter;
  return hiddenGetAllAccesibleAndRemove(ter, pos, nbCell, toRemove, cond);
}

arrayList getAllAccesibleAndRemoveMonster(terrain ter, position pos, int nbCell, linkedList toRemove){
  static boolean (*cond) (cell ) = &conditionForGetAccesibleMonster;
  return hiddenGetAllAccesibleAndRemove(ter, pos, nbCell, toRemove, cond);
}

typedef struct{
  terrain ter;
  linkedList beforeList;
} dataPositionCompare;


int positionCompare(void * elemA, void * elemB, void * plus){
  dataPositionCompare data = *((dataPositionCompare *) plus);
  terrain ter = data.ter;
  position posA = *((position *) elemA);
  position posB = *((position *) elemB);

  map mapVoisin = getAllNearWalkableNotSeenPos(ter, posA);
  int nbVoisinA;
  int nbVoisinB;
  collectionRemoveAll(arrayListAsCollection(&(mapVoisin.valueList)),
		      linkedListAsCollection(&(data.beforeList)));
  nbVoisinA = arrayListLength(mapVoisin.valueList);
  mapDestroy(&mapVoisin);
  
  mapVoisin = getAllNearWalkableNotSeenPos(ter, posB);
  collectionRemoveAll(arrayListAsCollection(&(mapVoisin.valueList)),
		      linkedListAsCollection(&(data.beforeList)));
  nbVoisinB = arrayListLength(mapVoisin.valueList);
  mapDestroy(&mapVoisin);
  /*
  if(isInBorderOf(ter, posA)) return -1;
  if(isInBorderOf(ter, posB)) return 1;
  */
  /* 1er critère : Est ce que y'en a un qui a plus qu'un voisin */

  if(nbVoisinA != nbVoisinB){
    if(nbVoisinA == 1) return -1;
    if(nbVoisinB == 1) return 1;
  }

  /* 2nd critère : Est ce que l'un est sur le bord de la map */

  /* 3eme critère : Est ce que l'un d'entre eux a que 2 voisins */

  if(nbVoisinA != nbVoisinB && nbVoisinA == 2) return -1;
  else if(nbVoisinA != nbVoisinB && nbVoisinB == 2) return 1;
  else{
    arrayList listA = getAllAccesibleAndRemoveMonster(ter, posA, 3, data.beforeList);
    arrayList listB = getAllAccesibleAndRemoveMonster(ter, posB, 3, data.beforeList);
    int sizeA = arrayListLength(listA);
    int sizeB = arrayListLength(listB);
    arrayListDestroy(&listA);
    arrayListDestroy(&listB);
    return sizeA - sizeB;
  }
}

typedef enum {OK, PATH_IMPOSSIBLE, PATH_OOT_EXCEPTION} getPathResEnum;

typedef struct{
  getPathResEnum functionStatus;
  linkedList list;
  long nbIter;
} getPathRes;

static getPathRes getPathHidden(terrain ter, int nbCell, linkedList beforePath, position pos){
  getPathRes res = {OK, NULL, 0};
  map mapVoisin;
  arrayList voisins;
  int (*compare)(void *, void *, void *) = positionCompare;
  dataPositionCompare data;
  boolean fini = false;
  int i;
  if(nbCell == 0) return res;
  mapVoisin = getAllNearWalkableNotSeenPos(ter, pos);
  voisins = mapVoisin.valueList;
  linkedListDestroy((mapVoisin.keyList));

  if(pos.x == 7 && pos.y == 2){
    free(NULL);
    free(NULL);
  }

  /* Enlever tout ceux déjà présent dans le chemin */
  collectionRemoveAll(arrayListAsCollection(&voisins),
		      linkedListAsCollection(&beforePath));

  /* Ajouter la position actuel */
  linkedListAddObj(&beforePath, &pos, sizeof(position));

  data.ter = ter;
  data.beforeList = beforePath;

  /* Tri de la liste */
  arrayListSort(voisins, compare, &data);
  
  for(i = 0; i < arrayListLength(voisins) && !fini; i++){

    position positionVoisin = *((position *) arrayListGet(voisins, i).pointer);

    /* On lance la ptit reccursion */
    getPathRes voisinRes = getPathHidden(ter, nbCell - 1, beforePath, positionVoisin);
    linkedListAddObj(&(voisinRes.list), &positionVoisin, sizeof(position));

    if(voisinRes.functionStatus == OK){
      
      /* Si c'est ok, bah on lance en prod mdr */
      linkedListDestroy(res.list);
      res.list = voisinRes.list;
      fini = true;
      
    }else if(voisinRes.functionStatus == PATH_OOT_EXCEPTION){

      /* Si on a trop fait d'iteration, on garde le meilleur chemin */
      res.functionStatus = PATH_OOT_EXCEPTION;
      if(linkedListLength(voisinRes.list) > linkedListLength(res.list)){
	linkedListDestroy(res.list);
	res.list = voisinRes.list;
      }else{
	linkedListDestroy(voisinRes.list);
      }
      fini = true;
    } else {
      /* Sinon, on incrémente le compteur et c'est reparti */
      if(linkedListLength(voisinRes.list) > linkedListLength(res.list)){
	linkedListDestroy(res.list);
	res.list = voisinRes.list;
      }else{
	linkedListDestroy(voisinRes.list);
      }
      if(res.nbIter >= 500){
	res.functionStatus = PATH_OOT_EXCEPTION;
	fini = true;
      }
    }
    if(voisinRes.functionStatus != PATH_OOT_EXCEPTION) res.nbIter += 1 + voisinRes.nbIter;
    else res.nbIter = voisinRes.nbIter;
  }

  if(!fini) res.functionStatus = PATH_IMPOSSIBLE;

  arrayListDestroy(&voisins);
  linkedListRemoveObj(&beforePath, &pos, sizeof(position));
  
  return res;
  
}

boolean isInBorderOf(terrain ter, position pos){
  return pos.x == 0
    || pos.y == 0
    || pos.x == ter.nbCellX - 1
    || pos.y == ter.nbCellY - 1;
}

linkedList getPath(terrain ter, int nbCell, position cellPos){
  getPathRes res = getPathHidden(ter, nbCell, NULL, cellPos);
  printf("\nD'en haut a en bas, il a fallu %ld iteration, res : %d\n\n", res.nbIter, res.functionStatus);
  return res.list;
}

typedef struct{
  boolean ifMonsterWithCell;
  union{
    char * (*withCell)(cell, int);
    char * (*withoutCell)(int);
  }ifMonster;
  boolean ifMonsterNearWithCell;
  union{
    char * (*withCell)(cell, int);
    char * (*withoutCell)(int);
  }ifMonsterNear;
  char * (*ifNotMonster)(cell, int);
}drawBoardVar;

static char * cellDrawMonsterInIt(int nbChar){
  static const char renderMonster = 'M';
  char * res = (char *) (calloc(nbChar + 1, sizeof(char)));
  memset(res, renderMonster, nbChar);
  res[nbChar] = '\0';
  return res;
}

/**
 * \fn static void drawBoardFonction(terrain ter, int nbCell, char * (*drawFonction)(cell, int))
 * \brief Affiche a l'écran le terrain avec une fonction de représentation donnée
 * \param ter Le terrain
 * \param nbCell Le nombre de caractère utilisé pour représentaer une case
 * \param drawFonction une fonction prenant en paramètre une cellule, un nombre de char et retourne la représentation de cette cellule avec le nombre de char donnée (malloced)
 */
static void drawBoardFonction(terrain ter, int nbChar, drawBoardVar var){
  static const char separator[] = "|";
  int y;
  for(y = 0; y < ter.nbCellY; y++){
    int x;
    printf("%s", separator);
    for(x = 0; x < ter.nbCellX; x++){
      char * toDraw;
      if(ter.monstre.pos.x == x && ter.monstre.pos.y == y){
	if(var.ifMonsterWithCell){
	  toDraw = (*(var.ifMonster.withCell))(*getCellXY(ter, x, y), nbChar);
	}else{
	  toDraw = (*(var.ifMonster.withoutCell))(nbChar);
	}
      }else{
	map allNear = getAllNearPos(ter, ter.monstre.pos);
	position posCell = createPosition(x, y);
	if(arrayListIndexOf(allNear.valueList, &posCell, sizeof(position)) != -1){
	  if(var.ifMonsterNearWithCell){
	    toDraw = (*(var.ifMonsterNear.withCell))(*getCellPos(ter, posCell), nbChar);
	  }else{
	    toDraw = (*(var.ifMonsterNear.withoutCell))(nbChar);
	  }
	}else{
	  toDraw = (*(var.ifNotMonster))(*getCellXY(ter, x, y), nbChar);
	}
	mapDestroy(&allNear);
      }
      printf("%s%s", toDraw, separator);
      free(toDraw);
    }
    printf("\n");
  }
}

void drawBoardHunter(terrain ter, int nbChar){
  static const drawBoardVar var = {true, {&cellCharForHunter}, true, {&cellCharForHunter},&cellCharForHunter};
  drawBoardFonction(ter, nbChar, var);
}

void drawBoardMonster(terrain ter, int nbChar){
  static const drawBoardVar var = {false, {(char * (*) (cell, int)) &cellDrawMonsterInIt}, true, {&cellCharForMonster}, &cellCharForMonster};
  drawBoardFonction(ter, nbChar, var);
}

void drawBoardMonsterComplete(terrain ter, int nbChar){
  static const drawBoardVar var = {false, {(char * (*) (cell, int))&cellDrawMonsterInIt}, true, {&cellCharForMonsterComplete}, &cellCharForMonster};
  drawBoardFonction(ter, nbChar, var);
}

void monsterMoveToPos(terrain * ter, position pos){
  if(getCellPos(*ter, pos)->typeCell != WALL){
    ter->monstre.pos = pos;
  }
}

void monsterMoveToDir(terrain * ter, direction dir){
  monsterMoveToPos(ter, getAddDir(ter->monstre.pos, dir));
}

void monsterMoveToXY(terrain * ter, int x, int y){
  monsterMoveToPos(ter, createPosition(x, y));
}

cell * monsterGetCell(terrain ter){
  return getCellPos(ter, ter.monstre.pos);
}

void monsterPlay(terrain * ter){
  int nb = monsterGetCell(*ter)->turn;
  if(ter->monstre.isAI){
    arrayList allPossible = getAllPosNotSeenTer(*ter);
    linkedList res = getPath(*ter, arrayListLength(allPossible), ter->monstre.pos);
    if(!linkedListIsEmpty(res)){
      monsterMoveToPos(ter, *((position *) linkedListGetIdx(res, 0).pointer));
    }
    arrayListDestroy(&allPossible);
    linkedListDestroy(res);
    monsterGetCell(*ter)->turn = nb + 1;
  }
}

arrayList getAllPosWalkableTer(terrain ter){
  arrayList res = arrayListCreate();
  int x;
  for(x = 0; x < ter.nbCellX; x++){
    int y;
    for(y = 0; y < ter.nbCellY; y++){
      if(ter.cells[y][x].typeCell != WALL){
	position pos = createPosition(x, y);
	arrayListAddObj(&res, &pos, sizeof(position));
      }
    }
  }
  return res;
}

arrayList getAllPosNotSeenTer(terrain ter){
  arrayList res = arrayListCreate();
  arrayList allWalkable = getAllPosWalkableTer(ter);
  position ** entry;
  FOREACHARRAYLIST(entry, allWalkable){
    if(!hasBeenVisited(*getCellPos(ter, **entry))){
      arrayListAddObj(&res, entry, sizeof(position));
    }
  }
  arrayListDestroy(&allWalkable);
  return res;
}

void onWalked(terrain * ter){
  cell monsterCell = *getCellPos(*ter, ter->monstre.pos);
  if(monsterCell.typeCell == STUNT_TRAP){
    ter->monstre.isFree = true;
    if(!(ter->monstre.isAI)){
      printf("Vous êtes tombés sur un piège, vous ne pourrez jouer au prochain tour.\n");
      passLine();
    }
  }else if(monsterCell.typeCell == TP_CELL){
    arrayList allNotSeen = getAllPosNotSeenTer(*ter);
    arrayListRemoveObj(&allNotSeen, &(ter->monstre.pos), sizeof(position));
    ter->monstre.pos = *((position *) arrayListGetRandom(allNotSeen).pointer);
    if(!(ter->monstre.isAI)){
      printf("Vous avez été téléporté vers une nouvelle case :\n");
      drawBoardMonster(*ter, 2);
      passLine();
    }
    arrayListDestroy(&allNotSeen);
  }else if(monsterCell.typeCell == VIEW_POINT_CELL){
    if(!(ter->monstre.isAI)){
      printf("Voici le terrain de manière complète :\n");
      drawBoardMonsterComplete(*ter, 2);
      passLine();
    }
  }
}
