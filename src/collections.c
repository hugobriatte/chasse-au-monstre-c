#include "collections.h"

collectionEntry collectionGet(collection col, int idx){
  if(col.type == 0) return arrayListGet(*(col.list.array), idx);
  else return linkedListGetIdx(*(col.list.linked), idx);
}

int collectionLength(collection col){
  if(col.type == 0) return arrayListLength(*(col.list.array));
  else return linkedListLength(*(col.list.linked));
}

void collectionAddAll(collection dest, collection src){
  int i;
  for(i = 0; i < collectionLength(src); i++){
    collectionEntry toAdd = collectionGet(src, i);
    if(dest.type == 0) arrayListAddObj(dest.list.array, toAdd.pointer, toAdd.size);
    else linkedListAddObj(dest.list.linked, toAdd.pointer, toAdd.size);
  }
}

void collectionRemoveAll(collection removeIn, collection toRemove){
  int i;
  for(i = 0; i < collectionLength(toRemove); i++){
    collectionEntry toAdd = collectionGet(toRemove, i);
    if(removeIn.type == 0) arrayListRemoveObj(removeIn.list.array, toAdd.pointer, toAdd.size);
    else linkedListRemoveObj(removeIn.list.linked, toAdd.pointer, toAdd.size);
  }
}

collection linkedListAsCollection(linkedList * list){
  collection res;
  res.type = 1;
  res.list.linked = list;
  return res;
}

collection arrayListAsCollection(arrayList * list){
  collection res;
  res.type = 0;
  res.list.array = list;
  return res;
}
