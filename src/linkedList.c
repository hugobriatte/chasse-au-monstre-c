#include "linkedList.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int linkedListLength(linkedList list){
  static int index = -1;
  index +=1;
  if(list == NULL){
    int reponse = index;
    index = -1;
    return reponse;
  }else{
    return linkedListLength(list->next);
  }
}

boolean linkedListIsEmpty(linkedList list){
  if(linkedListLength(list) == 0) return true;
  return false;
}

collectionEntry linkedListHead(linkedList list){
  if(linkedListIsEmpty(list)) return emptyCollectionEntry;
  else return list->entry;
}

linkedList linkedListQueue(linkedList list){
  if(linkedListIsEmpty(list)) return NULL;
  else return list->next;
}

struct linkedListEntry linkedListAssembleCollectionEntry(collectionEntry head, linkedList tail){
  struct linkedListEntry res;
  res.entry = head;
  res.next = tail;
  return res;
}

struct linkedListEntry linkedListAssemble(void * object, int sizeOf, linkedList tail){
  collectionEntry entry;
  void * pointer = malloc(sizeOf);
  memcpy(pointer, object, sizeOf);
  entry.pointer = pointer;
  entry.size = sizeOf;
  return linkedListAssembleCollectionEntry(entry, tail);
}

void linkedListAddObj(linkedList * list, void * object, int sizeOf){
  linkedList pointer = (linkedList) malloc(sizeof(struct linkedListEntry));
  *pointer = linkedListAssemble(object, sizeOf, *list);
  *list = pointer;
}

void linkedListAddObjIdx(linkedList * list, int idx, void * object, int sizeOf){
  if(idx == 0 || linkedListIsEmpty(*list)) linkedListAddObj(list, object, sizeOf);
  else linkedListAddObjIdx(&((**list).next), idx - 1, object, sizeOf);
}

collectionEntry linkedListGetIdx(linkedList list, int index){
  if(linkedListIsEmpty(list) || index < 0) return emptyCollectionEntry;
  else if(index == 0) return linkedListHead(list);
  else return linkedListGetIdx(linkedListQueue(list), index - 1);
}

void linkedListRemoveObj(linkedList * list, void * object, int sizeOf){
  if(!linkedListIsEmpty(*list)){
    struct linkedListEntry entry = **list;
    collectionEntry data = entry.entry;
    if(sizeOf == data.size && memcmp(data.pointer, object, sizeOf) == 0){
      free(data.pointer);
      free(*list);
      *list = entry.next;
    }else linkedListRemoveObj(&((**list).next), object, sizeOf);
  }
}

void linkedListRemoveIdx(linkedList * list, int index){
  if(!linkedListIsEmpty(*list) && index == 0){
    struct linkedListEntry entry = **list;
    collectionEntry data = entry.entry;
    free(data.pointer);
    free(*list);
    *list = entry.next;
  }else if(!linkedListIsEmpty(*list) && index > 0){
    linkedListRemoveIdx(&((**list).next), index - 1);
  }
}

int linkedListIndexOf(linkedList list, void * object, int sizeOf){
  static int index = -1;
  collectionEntry data = linkedListHead(list);
  index += 1;
  if(linkedListIsEmpty(list)){
    index = -1;
    return -1;
  }
  if(sizeOf == data.size && memcmp(data.pointer, object, sizeOf) == 0){
    int reponse = index;
    index = -1;
    return reponse;
  }else return linkedListIndexOf(linkedListQueue(list), object, sizeOf);
}

void linkedListDestroy(linkedList list){
  if(!linkedListIsEmpty(list)){
    collectionEntry head = linkedListHead(list);
    linkedList next = linkedListQueue(list);
    free(head.pointer);
    free(list);
    linkedListDestroy(next);
  }
}
