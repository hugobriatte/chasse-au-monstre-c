#include "linkedList.h"

set setCreate(){
  set set = {NULL};
  return set;
}

boolean setIsEmpty(set set){
  return linkedListIsEmpty(set.list);
}

int setLength(set set){
  return linkedListLength(set.list);
}

boolean setContains(set set, void * object, int sizeOf){
  return linkedListIndexOf(set.list, object, sizeOf) != -1;
}

collectionEntry setAdd(set * set, void * object, int sizeOf){
  if(!setContains(*set, object, sizeOf)){
    linkedListAddObj(&(set.list), object, sizeOf);
  }
}

void setRemove(set * set, void * object, int sizeOf){
  linkedListRemoveObj(&(set.list), object, sizeOf);
}

void setDestroy(set set){
  linkedListDestroy(set.list);
}
