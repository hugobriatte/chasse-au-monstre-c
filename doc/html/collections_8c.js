var collections_8c =
[
    [ "arrayListAsCollection", "collections_8c.html#a2a5cd681ab7e288e68b67ee561385291", null ],
    [ "collectionAddAll", "collections_8c.html#ae11053cf1eb5483209f3931da5f0adad", null ],
    [ "collectionGet", "collections_8c.html#a83bdd629dc7b65fa3ebed97472c12e68", null ],
    [ "collectionLength", "collections_8c.html#ac27149206a07b10574bc24416e652c0d", null ],
    [ "collectionRemoveAll", "collections_8c.html#a4c8662acdcd2faf553dac8272b3b3399", null ],
    [ "linkedListAsCollection", "collections_8c.html#a84eee1ddd16d307d70b1bca6f3575976", null ]
];