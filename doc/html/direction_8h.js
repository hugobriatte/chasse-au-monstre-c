var direction_8h =
[
    [ "direction", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098", [
      [ "LEFT_UP", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098a68d2cceb071f30c4b3ffbefdf0499137", null ],
      [ "LEFT", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098adb45120aafd37a973140edee24708065", null ],
      [ "LEFT_DOWN", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098aaf7c118e8e01fba26de9975c8bb8b1a6", null ],
      [ "UP", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098aba595d8bca8bc5e67c37c0a9d89becfa", null ],
      [ "NONE", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098ac157bdf0b85a40d2619cbc8bc1ae5fe2", null ],
      [ "DOWN", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098a9b0b4a95b99523966e0e34ffdadac9da", null ],
      [ "RIGHT_UP", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098aab50c2b56b93fa86d463d070cf63ab55", null ],
      [ "RIGHT", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098aec8379af7490bb9eaaf579cf17876f38", null ],
      [ "RIGHT_DOWN", "direction_8h.html#a99f26e6ee9fcd62f75203b5402df8098aa5f8da60a088071fe87d5d31be4842ec", null ]
    ] ],
    [ "getDirX", "direction_8h.html#ac4a8f387fe6c6f606107ce9de4e3ad7c", null ],
    [ "getDirY", "direction_8h.html#a7a3446c8e229810fa84357d7d4d4d5a7", null ],
    [ "readDirection", "direction_8h.html#ab9e295c84b7348cce6ac15d7370d1f2f", null ],
    [ "readDirectionOf", "direction_8h.html#aee9988351926bc514afe57dcc3860d15", null ]
];