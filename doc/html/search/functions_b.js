var searchData=
[
  ['terraincreatewith',['terrainCreateWith',['../terrain_8h.html#a62293b798eae2f0aee792f96b6eca131',1,'terrainCreateWith(int nbCellX, int nbCellY, boolean isThereWall, boolean isThereViewPoint, boolean isThereTp, boolean monsterAI):&#160;terrain.c'],['../terrain_8c.html#a6ce417a458ba8f891940aaec2882cc9d',1,'terrainCreateWith(int nbCellX, int nbCellY, boolean isThereWall, boolean isThereViewPoint, boolean isThereTpCell, boolean monsterAI):&#160;terrain.c']]],
  ['terraindestroy',['terrainDestroy',['../terrain_8h.html#a5bba9e44fc90459dd2c700777724fa5b',1,'terrainDestroy(terrain ter):&#160;terrain.c'],['../terrain_8c.html#a5bba9e44fc90459dd2c700777724fa5b',1,'terrainDestroy(terrain ter):&#160;terrain.c']]],
  ['translatewithint',['translateWithInt',['../position_8h.html#a56965b81504a7c7f08786d4f2b9ffae2',1,'translateWithInt(position *pos, int x, int y):&#160;position.c'],['../position_8c.html#a56965b81504a7c7f08786d4f2b9ffae2',1,'translateWithInt(position *pos, int x, int y):&#160;position.c']]],
  ['translatewithpos',['translateWithPos',['../position_8h.html#a912df2fad84d1167d886d159456f578d',1,'translateWithPos(position *posOrigin, position vector):&#160;position.c'],['../position_8c.html#a0fa7d6f75d39fe5f02ba583d7c960c4d',1,'translateWithPos(position *pos, position vector):&#160;position.c']]]
];
