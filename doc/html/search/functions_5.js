var searchData=
[
  ['iscompletelyvisited',['isCompletelyVisited',['../terrain_8h.html#aefd686d69319969a5a008a9bae973aac',1,'isCompletelyVisited(terrain ter):&#160;terrain.c'],['../terrain_8c.html#aefd686d69319969a5a008a9bae973aac',1,'isCompletelyVisited(terrain ter):&#160;terrain.c']]],
  ['isinborderof',['isInBorderOf',['../terrain_8c.html#a3d5facaddb5e4a0a49fc6a973f6d5f23',1,'terrain.c']]],
  ['isposvalid',['isPosValid',['../terrain_8h.html#aa227ec81fd5f363c115fb479b410d600',1,'isPosValid(terrain ter, position pos):&#160;terrain.c'],['../terrain_8c.html#aa227ec81fd5f363c115fb479b410d600',1,'isPosValid(terrain ter, position pos):&#160;terrain.c']]],
  ['isxyvalid',['isXYValid',['../terrain_8h.html#acb762cbd3e7df8b89e5a568d3e976b8d',1,'isXYValid(terrain ter, int x, int y):&#160;terrain.c'],['../terrain_8c.html#acb762cbd3e7df8b89e5a568d3e976b8d',1,'isXYValid(terrain ter, int x, int y):&#160;terrain.c']]]
];
