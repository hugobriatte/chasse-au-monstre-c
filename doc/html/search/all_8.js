var searchData=
[
  ['ifmonster',['ifMonster',['../structdraw_board_var.html#a3fa6193bdc7d9adbef852bd8f66fc7b0',1,'drawBoardVar']]],
  ['ifnotmonster',['ifNotMonster',['../structdraw_board_var.html#a0613d1e0811819f8d8e911152c1aecc7',1,'drawBoardVar']]],
  ['isai',['isAI',['../structhunter.html#aa6358a1b8ac34eac2b2dd24db9d5ccf8',1,'hunter::isAI()'],['../structmonster.html#aa6358a1b8ac34eac2b2dd24db9d5ccf8',1,'monster::isAI()']]],
  ['iscompletelyvisited',['isCompletelyVisited',['../terrain_8h.html#aefd686d69319969a5a008a9bae973aac',1,'isCompletelyVisited(terrain ter):&#160;terrain.c'],['../terrain_8c.html#aefd686d69319969a5a008a9bae973aac',1,'isCompletelyVisited(terrain ter):&#160;terrain.c']]],
  ['isfree',['isFree',['../structmonster.html#a6f08b3173dd7e1f643b1e37cadff9b80',1,'monster']]],
  ['isinborderof',['isInBorderOf',['../terrain_8c.html#a3d5facaddb5e4a0a49fc6a973f6d5f23',1,'terrain.c']]],
  ['isposvalid',['isPosValid',['../terrain_8h.html#aa227ec81fd5f363c115fb479b410d600',1,'isPosValid(terrain ter, position pos):&#160;terrain.c'],['../terrain_8c.html#aa227ec81fd5f363c115fb479b410d600',1,'isPosValid(terrain ter, position pos):&#160;terrain.c']]],
  ['isxyvalid',['isXYValid',['../terrain_8h.html#acb762cbd3e7df8b89e5a568d3e976b8d',1,'isXYValid(terrain ter, int x, int y):&#160;terrain.c'],['../terrain_8c.html#acb762cbd3e7df8b89e5a568d3e976b8d',1,'isXYValid(terrain ter, int x, int y):&#160;terrain.c']]]
];
