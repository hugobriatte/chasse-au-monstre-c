var searchData=
[
  ['terraincreate',['terrainCreate',['../terrain_8h.html#a2d001bedca685aa258535880d9493f08',1,'terrainCreate(int nbCellX, int nbCellY):&#160;terrain.c'],['../terrain_8c.html#a2d001bedca685aa258535880d9493f08',1,'terrainCreate(int nbCellX, int nbCellY):&#160;terrain.c']]],
  ['terraindestroy',['terrainDestroy',['../terrain_8h.html#a5bba9e44fc90459dd2c700777724fa5b',1,'terrainDestroy(terrain ter):&#160;terrain.c'],['../terrain_8c.html#a5bba9e44fc90459dd2c700777724fa5b',1,'terrainDestroy(terrain ter):&#160;terrain.c']]],
  ['translatewithint',['translateWithInt',['../position_8h.html#a56965b81504a7c7f08786d4f2b9ffae2',1,'translateWithInt(position *pos, int x, int y):&#160;position.c'],['../position_8c.html#a56965b81504a7c7f08786d4f2b9ffae2',1,'translateWithInt(position *pos, int x, int y):&#160;position.c']]],
  ['translatewithpos',['translateWithPos',['../position_8h.html#a912df2fad84d1167d886d159456f578d',1,'translateWithPos(position *posOrigin, position vector):&#160;position.c'],['../position_8c.html#a0fa7d6f75d39fe5f02ba583d7c960c4d',1,'translateWithPos(position *pos, position vector):&#160;position.c']]]
];
