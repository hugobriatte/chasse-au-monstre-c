var collections_8h =
[
    [ "collection", "structcollection.html", "structcollection" ],
    [ "arrayListAsCollection", "collections_8h.html#a2a5cd681ab7e288e68b67ee561385291", null ],
    [ "collectionAddAll", "collections_8h.html#ae11053cf1eb5483209f3931da5f0adad", null ],
    [ "collectionRemoveAll", "collections_8h.html#a4c8662acdcd2faf553dac8272b3b3399", null ],
    [ "linkedListAsCollection", "collections_8h.html#a84eee1ddd16d307d70b1bca6f3575976", null ]
];