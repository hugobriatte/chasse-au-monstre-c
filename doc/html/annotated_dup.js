var annotated_dup =
[
    [ "arrayList", "structarray_list.html", "structarray_list" ],
    [ "cell", "structcell.html", "structcell" ],
    [ "collection", "structcollection.html", "structcollection" ],
    [ "collectionEntry", "structcollection_entry.html", "structcollection_entry" ],
    [ "dataPositionCompare", "structdata_position_compare.html", "structdata_position_compare" ],
    [ "drawBoardVar", "structdraw_board_var.html", "structdraw_board_var" ],
    [ "getPathRes", "structget_path_res.html", "structget_path_res" ],
    [ "hunter", "structhunter.html", "structhunter" ],
    [ "linkedListEntry", "structlinked_list_entry.html", "structlinked_list_entry" ],
    [ "list", "structlist.html", null ],
    [ "map", "structmap.html", "structmap" ],
    [ "monster", "structmonster.html", "structmonster" ],
    [ "position", "structposition.html", "structposition" ],
    [ "set", "structset.html", "structset" ],
    [ "terrain", "structterrain.html", "structterrain" ]
];