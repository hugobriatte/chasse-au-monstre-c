#include "terrain.h"
#include <stdio.h>

int main(int argc, const char* argv[]){
  terrain ter = terrainCreate(7, 7, false);
  if(argc >= 4){
    position pos;
    linkedList res;
    int i;
    pos.x = atoi(argv[1]);
    pos.y = atoi(argv[2]);
    res = getPath(ter, atoi(argv[3]), pos);
    for(i = 0; i < linkedListLength(res); i++){
      position newPos = *((position *) linkedListGetIdx(res, i).pointer);
      printf("{%d,%d}\n",newPos.x,newPos.y);
    }
    linkedListDestroy(res);
  }
  terrainDestroy(ter);

  return 0;
}
