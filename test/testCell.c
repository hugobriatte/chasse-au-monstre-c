#include "cell.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
int main(){
  int i;
  cell toTest;
  cell wallCell = createCell(WALL, -1),
    walkableCell = createCell(WALKABLE_CELL, -1),
    viewPointCell = createCell(VIEW_POINT_CELL, -1),
    stuntTrapCell = createCell(STUNT_TRAP, -1),
    tpCell = createCell(TP_CELL, -1);
  cell invisibleForMonster[4];
  invisibleForMonster[0] = walkableCell;
  invisibleForMonster[1] = viewPointCell;
  invisibleForMonster[2] = stuntTrapCell;
  invisibleForMonster[3] = tpCell;
  /* Test hasBeenVisited */
  for(i = 1; i < 10; i++) {
    toTest = createCell(WALKABLE_CELL, i);
    assert(hasBeenVisited(toTest));
  }
  
  toTest = createCell(WALKABLE_CELL, -1);
  assert(!hasBeenVisited(toTest));

  /* Test cellCharForHunter */
  assert(strcmp("#", cellCharForHunter(wallCell, 1)) == 0);
  assert(strcmp("^", cellCharForHunter(viewPointCell, 1)) == 0);
  assert(strcmp("~", cellCharForHunter(stuntTrapCell, 1)) == 0);
  assert(strcmp(".", cellCharForHunter(tpCell, 1)) == 0);
  assert(strcmp(" ", cellCharForHunter(walkableCell, 1)) == 0);

  /* Test cellCharForMonster*/
  for(i = 0; i < 4; i++){
    assert(strcmp(" ", cellCharForMonster(invisibleForMonster[i], 1)) == 0);
    invisibleForMonster[i].turn = 3;
    assert(strcmp("3", cellCharForMonster(invisibleForMonster[i], 1)) == 0);
    assert(strcmp("3  ", cellCharForMonster(invisibleForMonster[i], 3)) == 0);
  }
  assert(strcmp("#", cellCharForMonster(wallCell, 1)) == 0);
  assert(strcmp("###", cellCharForMonster(wallCell, 3)) == 0);

  /* Test cellCharForMonsterComplete */
  for(i = 0; i < 4; i++) {
    assert(strcmp("3", cellCharForMonster(invisibleForMonster[i], 1)) == 0);
    assert(strcmp("3  ", cellCharForMonster(invisibleForMonster[i], 3)) == 0);
    invisibleForMonster[i].turn = -1;
    assert(strcmp(cellCharForMonsterComplete(invisibleForMonster[i], i), cellCharForHunter(invisibleForMonster[i], i)) == 0);
  }

  return 0;
}
