#include "collections.h"
#include "assert.h"

int main(){
  arrayList arrayList = arrayListCreate();
  linkedList linkedList = NULL;
  int a1 = 5;
  char a2 = 15;
  int al = 3;
  short l1 = 259;
  collection arrayCollection;
  collection linkedCollection;
  arrayListAddObj(&arrayList, &a1, sizeof(int));
  arrayListAddObj(&arrayList, &a2, sizeof(char));
  arrayListAddObj(&arrayList, &al, sizeof(int));
  linkedListAddObj(&linkedList, &al, sizeof(int));
  linkedListAddObj(&linkedList, &l1, sizeof(short));

  /* Test ...ListAsCollection */
  arrayCollection = arrayListAsCollection(&arrayList);
  assert(arrayCollection.type == 0);
  assert(arrayCollection.list.array == &arrayList);

  linkedCollection = linkedListAsCollection(&linkedList);
  assert(linkedCollection.type == 1);
  assert(linkedCollection.list.linked == &linkedList);

  /* Test collectionAddAll */
  collectionAddAll(linkedCollection, arrayCollection);
  assert(linkedListIndexOf(linkedList, &a1, sizeof(int)) != -1);
  assert(linkedListIndexOf(linkedList, &a2, sizeof(char)) != -1);

  /* Test collectionRemoveAll */
  collectionRemoveAll(arrayCollection, linkedCollection);
  assert(arrayListIndexOf(arrayList, &a1, sizeof(int)) == -1);
  assert(arrayListIndexOf(arrayList, &a2, sizeof(char)) == -1);
  assert(arrayListIndexOf(arrayList, &al, sizeof(int)) == -1);
  assert(arrayListIndexOf(arrayList, &l1, sizeof(short)) == -1);

  linkedListDestroy(linkedList);
  arrayListDestroy(&arrayList);

  return 0;
}
