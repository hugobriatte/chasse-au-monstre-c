#include "direction.h"
#include <assert.h>
#include <stdio.h>
void testGetDirXY(){
  direction up[] = {LEFT_UP, UP, RIGHT_UP};
  direction down[] = {LEFT_DOWN, DOWN, RIGHT_DOWN};
  direction left[] = {LEFT_DOWN, LEFT, LEFT_UP};
  direction right[] = {RIGHT_UP, RIGHT, RIGHT_DOWN};
  direction upp = UP;
  direction downn = DOWN;
  direction leftt = LEFT;
  direction rightt = RIGHT;
  int i;
  for(i = 0; i < 3; i++){
    assert(getDirY(up[i]) == -1);
    assert(getDirY(down[i]) == 1);
    assert(getDirX(left[i]) == -1);
    assert(getDirX(right[i]) == 1);
  }
  assert(getDirX(upp) == 0);
  assert(getDirX(downn) == 0);
  assert(getDirY(leftt) == 0);
  assert(getDirY(rightt) == 0);
}

void testReadDirection(){
  FILE * tmp = tmpfile();
  char * tab[9] = {"L-U", "L", "L-D", "U", "N", "D", "R-U", "R", "R-D"};
  int i;
  for(i = 0; i < 9; i++){
    fputs(tab[i], tmp);
    fputs("\n", tmp);
  }
  rewind(tmp);
  /*
  while(!feof(tmp)){
    printf("%c", getc(tmp));
  }
  */
  for(i = 0; i < 9; i++){
    direction d = (direction) i;
    assert(readDirectionOf(tmp) == d);
    getc(tmp);
  }
}

int main(){
  testGetDirXY();
  testReadDirection();
  return 0;
}
