#include "arrayList.h"
#include "position.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

void testA(){
  arrayList list;
  int cellule;
  int * res;
  int secondeCellule;
  int troisiemeCellule;
  
  /* Test create */
  list = arrayListCreate();
  assert(arrayListLength(list) == 0);
  assert(list.maxSize == 4);

  /* Test createWithSize */
  arrayListDestroy(&list);
  list = arrayListCreateWithSize(2);
  assert(arrayListLength(list) == 0);
  assert(list.maxSize == 2);
  
  /* Test isEmpty */
  cellule = 4;
  assert(arrayListIsEmpty(list));
  arrayListAddObj(&list, &cellule, sizeof(int));
  assert(!arrayListIsEmpty(list));

  /* Test get / addObj */
  res = (int *) arrayListGet(list, 0).pointer;
  assert(*res == cellule);
  res = (int *) arrayListGet(list, 1).pointer;
  assert(res == NULL);

  /* Test length */
  secondeCellule = 1;
  assert(arrayListLength(list) == 1);
  arrayListAddObj(&list, &secondeCellule, sizeof(int));
  assert(arrayListLength(list) == 2);

  /* Test indexOf */
  troisiemeCellule = 5;
  assert(arrayListIndexOf(list, &secondeCellule, sizeof(int)) == 1);
  assert(arrayListIndexOf(list, &cellule, sizeof(int)) == 0);
  assert(arrayListIndexOf(list, &troisiemeCellule, sizeof(int)) == -1);

  /* Test addObjIdx */
  arrayListAddObjIdx(&list, 1, &troisiemeCellule, sizeof(int));
  res = (int *) arrayListGet(list, 0).pointer;
  assert(*res == cellule);
  res = (int *) arrayListGet(list, 1).pointer;
  assert(*res == troisiemeCellule);
  res = (int *) arrayListGet(list, 2).pointer;
  assert(*res == secondeCellule);
  assert(arrayListLength(list) == 3);
  
  /* Test setObj */
  
  arrayListSetObj(list, 0, &secondeCellule, sizeof(int));
  res = (int *) arrayListGet(list, 0).pointer;
  assert(*res == secondeCellule);
 
  /* Test removeIdx */
 
  arrayListRemoveIdx(&list, 0);
  res = (int *) arrayListGet(list, 0).pointer;
  assert(*res == troisiemeCellule);
  res = (int *) arrayListGet(list, 1).pointer;
  assert(*res == secondeCellule);
  assert(arrayListLength(list) == 2);
 
  /* Test removeObj*/
 
  arrayListRemoveObj(&list, &troisiemeCellule, sizeof(int));
  res = (int *) arrayListGet(list, 0).pointer;
  assert(*res == secondeCellule);
  assert(arrayListLength(list) == 1);
 
  arrayListDestroy(&list);
}

void testB(){
  arrayList list = arrayListCreateWithSize(2);
  position posA = {2, 4};
  position posB = {-1, -1};
  arrayListAddObj(&list, &posA, sizeof(position));
  arrayListAddObj(&list, &posB, sizeof(position));
  arrayListAddObj(&list, &posB, sizeof(position));
  arrayListAddObj(&list, &posA, sizeof(position));
  arrayListAddObj(&list, &posB, sizeof(position));
  arrayListAddObj(&list, &posB, sizeof(position));
  arrayListDestroy(&list);
}

int main(){
  testA();
  return 0;
}
