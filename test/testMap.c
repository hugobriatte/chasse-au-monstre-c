#include "map.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

typedef enum {SEESAW, EFFECT} values;

char * getString(map map){
  values effect = EFFECT;
  return *((char **) mapGet(map, &effect, sizeof(values)).pointer);
}

int getInt(map map){
  values seesaw = SEESAW;
  return *((int *) mapGet(map, &seesaw, sizeof(values)).pointer);
}

int main(){

  /* Test enumMapCreate */
  map map = mapCreate();

  int effect = EFFECT;
  int seesaw = SEESAW;
  
  char * guilty = "Korekiyo";
  
  int tenko = 0xded;
  int hope = 11037;
  
  assert(linkedListIsEmpty(map.keyList));
  assert(arrayListIsEmpty(map.valueList));

  /* Test mapPut / mapGet */
  assert(mapGet(map, &effect, sizeof(values)).pointer == NULL);
  assert(mapGet(map, &seesaw, sizeof(values)).pointer == NULL);
  
  mapPut(&map, &effect, sizeof(values), &guilty, sizeof(char *));

  assert(getString(map) == guilty);
  assert(mapGet(map, &seesaw, sizeof(values)).pointer == NULL);

  mapPut(&map, &seesaw, sizeof(values), &tenko, sizeof(int));
  assert(getString(map) == guilty);
  assert(getInt(map) == tenko);

  assert(mapGet(map, &hope, sizeof(values)).pointer == NULL);

  /* Test mapRemove */
  mapRemove(&map, &seesaw, sizeof(values));
  assert(getString(map) == guilty);
  assert(mapGet(map, &seesaw, sizeof(values)).pointer == NULL);

  /* Test mapPlaced */
  assert(mapPlaced(map, &effect, sizeof(values)));
  assert(!mapPlaced(map, &seesaw, sizeof(values)));
  assert(!mapPlaced(map, &hope, sizeof(values)));

  mapDestroy(&map);

  return 0;
}
