#include "terrain.h"
#include <stdio.h>
#include <assert.h>

terrain ter;

void testCreateTerrain(){
  int x;
  int y;
  assert(ter.nbCellX == 3);
  assert(ter.nbCellY == 3);
  for(x = 0; x < ter.nbCellX; x++){
    for(y = 0; y < ter.nbCellY; y++){
      assert(ter.cells[y][x].typeCell == WALKABLE_CELL);
    }
  }
}

void testGetCell(){
  position pos = createPosition(4, 1);
  assert(getCellXY(ter, 1, 1) == ter.cells[1] + 1);
  assert(getCellPos(ter, pos) == NULL);
}

void testPlacePiege(){
  position pos = createPosition(1, 0);
  placePiegeXY(ter, 0, 0);
  placePiegePos(ter, pos);
  assert(getCellXY(ter, 0, 0)->typeCell == STUNT_TRAP);
  assert(getCellXY(ter, 1, 0)->typeCell == STUNT_TRAP);
}

void testIsValid(){
  position pos = createPosition(-1, -1);
  assert(!isPosValid(ter, pos));
  assert(isXYValid(ter, 2, 2));
}

void testGetAllNear(){
  position positionInitial = createPosition(1, 0);
  map mapAllNear = getAllNearPos(ter, positionInitial);
  direction dir;
  for(dir = 0; dir <= RIGHT_DOWN; dir++){
    if(dir != NONE && dir != LEFT_UP && dir != UP && dir != RIGHT_UP){
      position posAt = *((position *)mapGet(mapAllNear, &dir, sizeof(direction)).pointer);
      assert(mapPlaced(mapAllNear, &dir, sizeof(direction)));
      assert(positionEquals(posAt, getAddDir(positionInitial, dir)));
    }else{
      assert(!mapPlaced(mapAllNear, &dir, sizeof(direction)));
    }
  }

  mapDestroy(&mapAllNear);
}

void testGetAllNearWalkable(){

  /* Test getAllNearWalkable */

  map mapAllNearWalk;
  direction dir;
  position positionInitial = createPosition(1, 0);

  ter.cells[0][0].typeCell = WALL;
  ter.cells[1][0].typeCell = WALL;

  mapAllNearWalk = getAllNearWalkablePos(ter, positionInitial);
  for(dir = 0; dir <= RIGHT_DOWN; dir++){
    if(dir == DOWN || dir == RIGHT_DOWN || dir == RIGHT){
      position posAt = *((position *)mapGet(mapAllNearWalk, &dir, sizeof(direction)).pointer);
      assert(mapPlaced(mapAllNearWalk, &dir, sizeof(direction)));
      assert(positionEquals(posAt, getAddDir(positionInitial, dir)));
    }else{
      assert(!mapPlaced(mapAllNearWalk, &dir, sizeof(direction)));
    }
  }

  mapDestroy(&mapAllNearWalk);

}

void testGetAllNearWalkableNotSeen(){
  map mapAllNearWalkNotSeen;
  direction dir;
  position positionInitial = createPosition(1, 0);

  ter.cells[1][1].turn = 1;

  mapAllNearWalkNotSeen = getAllNearWalkableNotSeenPos(ter, positionInitial);
  for(dir = 0; dir <= RIGHT_DOWN; dir++){
    position posOfCell = getAddDir(positionInitial, dir);
    if((dir == RIGHT || dir == RIGHT_DOWN) && !positionEquals(ter.monstre.pos, posOfCell)){
      position posAt = *((position *)mapGet(mapAllNearWalkNotSeen, &dir, sizeof(direction)).pointer);
      assert(mapPlaced(mapAllNearWalkNotSeen, &dir, sizeof(direction)));
      assert(positionEquals(posAt, posOfCell));
    }else{
      assert(!mapPlaced(mapAllNearWalkNotSeen, &dir, sizeof(direction)));
    }
  }

  mapDestroy(&mapAllNearWalkNotSeen);
}

void testGetAllWalkableCellTer(){
  arrayList res = getAllPosWalkableTer(ter);
  int x;
  for(x = 0; x < ter.nbCellX; x++){
    int y;
    for(y = 0; y < ter.nbCellY; y++){
      position pos = createPosition(x, y);
      if(ter.cells[y][x].typeCell == WALL){
	assert(arrayListIndexOf(res, &pos, sizeof(position)) == -1);
      } else {
	assert(arrayListIndexOf(res, &pos, sizeof(position)) != -1);
      }
    }
  }
  arrayListDestroy(&res);
}

void testGetAllNotSeenTer(){
  arrayList res = getAllPosNotSeenTer(ter);
  arrayList allWalkable = getAllPosWalkableTer(ter);
  position ** entry;
  FOREACHARRAYLIST(entry, allWalkable){
    position pos = **entry;
    cell cel = *getCellPos(ter, pos);
    if(hasBeenVisited(cel)){
      assert(arrayListIndexOf(res, &pos, sizeof(position)) == -1);
    }else{
      assert(arrayListIndexOf(res, &pos, sizeof(position)) != -1);
    }
  }
  arrayListDestroy(&res);
  arrayListDestroy(&allWalkable);
}

void testMonsterPlay(){
  map mapMonster = getAllNearWalkableNotSeenPos(ter, ter.monstre.pos);
  arrayList res;
  while(!linkedListIsEmpty(mapMonster.keyList)){
    monsterPlay(&ter);
    printf("\n");
    drawBoardMonster(ter, 2);
    mapDestroy(&mapMonster);
    mapMonster = getAllNearWalkableNotSeenPos(ter, ter.monstre.pos);
  }
  mapDestroy(&mapMonster);
  res = getAllPosNotSeenTer(ter);
  if(arrayListIsEmpty(res)) printf("Ok\n");
  else printf("Nope\n");
  arrayListDestroy(&res);
}

int main() {
  ter = terrainCreateWith(3,3,false, false, false, true);
  testCreateTerrain();
  testGetCell();
  testPlacePiege();
  testIsValid();
  testGetAllNear();
  testGetAllNearWalkable();
  testGetAllNearWalkableNotSeen();
  
  terrainDestroy(ter);
  ter = terrainCreateWith(7,7,true, true, true, true);
  testMonsterPlay();

  terrainDestroy(ter);
  
  return 0;
}
