#include "position.h"
#include <assert.h>
#include <time.h>
#include <stdlib.h>

int main()
{
  int i;
  srand(time(NULL));

  /* Test getAddPos */
  for(i = 0; i < 100; i++){
    position posA, posB, excepted;
    posA.x = rand();
    posA.y = rand();
    posB.x = rand();
    posB.y = rand();
    excepted.x = posA.x + posB.x;
    excepted.y = posA.y + posB.y;
    assert(positionEquals(getAddPos(posA, posB),excepted));
  }

  /* Test getAddDir */
  for(i = 0; i < 9; i++){
    position pos, excepted;
    direction dir;
    dir = i;
    pos.x = rand();
    pos.y = rand();
    excepted = getAddPos(pos, getPosOf(dir));
    assert(positionEquals(getAddDir(pos,dir), excepted));
  }

  /* Test getAddXY */
  for(i = 0; i < 100; i++){
    position pos, excepted;
    int x, y;
    pos.x = rand();
    pos.y = rand();
    x = rand();
    y = rand();
    excepted.x = pos.x + x;
    excepted.y = pos.y + y;
    assert(positionEquals(getAddXY(pos,x,y), excepted));
  }

  /* Test getPosOf */
  for(i = 0; i < 9; i++){
    position excepted;
    direction dir = i;
    excepted.x = getDirX(dir);
    excepted.y = getDirY(dir);
    assert(positionEquals(getPosOf(dir), excepted));
  }

  /* Test positionEquals */
  for(i = 0; i < 100; i++){
    int x = rand(), y = rand();
    position posA, posB;
    posA.x = x;
    posA.y = y;
    posB.x = x;
    posB.y = y;
    assert(positionEquals(posA, posB));
    posB.x = posB.x + 2;
    assert(!positionEquals(posA, posB));
  }

  /* Test movePosToDir */
  for(i = 0; i < 9; i++){
    int x = rand(), y = rand();
    position pos = createPosition(x, y);
    direction dir = i;
    fixedPosition excepted = createPosition(x + getPosOf(dir).x, y + getPosOf(dir).y);
    movePosToDir(&pos, dir);
    assert(positionEquals(pos, excepted));
  }

  /* Test translateWithPos */
  for(i = 0; i < 100; i++){
    int x = rand(), y = rand();
    position pos = createPosition(x, y);
    int addX = rand(), addY = rand();
    fixedPosition vector = createPosition(addX, addY);
    fixedPosition excepted = createPosition(x + addX, y + addY);
    translateWithPos(&pos, vector);
    assert(positionEquals(pos, excepted));
  }

  /* Test translateWithInt */
  for(i = 0; i < 100; i++){
    int x = rand(), y = rand();
    position pos = createPosition(x, y);
    int addX = rand(), addY = rand();
    fixedPosition excepted = createPosition(x + addX, y + addY);
    translateWithInt(&pos, addX, addY);
    assert(positionEquals(pos, excepted));
  }
  
  return 0;
}
