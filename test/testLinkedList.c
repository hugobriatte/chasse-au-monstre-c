#include "linkedList.h"
#include <assert.h>
#include <stdio.h>

int main(){
  linkedList list = emptyLinkedList;
  int add1 = 25;
  long add2 = 0x11037;
  char add3 = 30;
  long object2;
  int object1;
  int ** e;

  /* Test isEmpty / addObj */
  assert(linkedListIsEmpty(list));
  linkedListAddObj(&list, &add1, sizeof(int));
  assert(!linkedListIsEmpty(list));

  /* Test length */
  assert(linkedListLength(list) == 1);
  linkedListAddObj(&list, &add2, sizeof(long));
  assert(linkedListLength(list) == 2);

  /* Test head */
  object2 = *((long *) linkedListHead(list).pointer);
  assert(object2 == add2);
  assert(linkedListHead(emptyLinkedList).pointer == emptyCollectionEntry.pointer);
  assert(linkedListHead(emptyLinkedList).size == emptyCollectionEntry.size);

  /* Test queue */
  object1 = *((int *) linkedListHead(linkedListQueue(list)).pointer);
  assert(object1 == add1);
  assert(linkedListQueue(emptyLinkedList) == emptyLinkedList);

  /* Test getIdx */
  object1 = *((int *) linkedListGetIdx(list, 1).pointer);
  assert(object1 == add1);
  assert(linkedListGetIdx(list, -5).pointer == emptyCollectionEntry.pointer);
  assert(linkedListGetIdx(list, 9).size == emptyCollectionEntry.size);

  /* Test removeObj / indexOf */
  linkedListAddObj(&list, &add3, sizeof(char));
  linkedListRemoveObj(&list, &add2, sizeof(long));
  assert(linkedListIndexOf(list, &add1, sizeof(int)) == 1);
  assert(linkedListIndexOf(list, &add2, sizeof(long)) == -1);
  assert(linkedListIndexOf(list, &add3, sizeof(char)) == 0);

  FOREACHLINKEDLIST(e, list){
    printf("%d\n", **e));
  }
  
  linkedListDestroy(list);

  return 0;
}
