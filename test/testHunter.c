#include "hunter.h"

int main(){

  hunter hunt = createHunter(true);
  terrain ter = terrainCreateWith(7, 7, true, false, false, true);
  int i;

  for(i = 2; i < 10; i++){
    position pos = hunterPlay(&hunt, ter, i);
    getCellPos(ter, pos)->turn = i;
    if(positionEquals(pos, ter.monstre.pos)) printf("Monstre dessu ");
    printf("%i\n", getCellPos(ter, pos)->typeCell);
  }

  drawBoardMonster(ter, 2);
  hunterDestroy(hunt);
  terrainDestroy(ter);
  return 0;
}
