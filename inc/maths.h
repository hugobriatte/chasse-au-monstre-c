#ifndef IMPORT_MATHS
#define IMPORT_MATHS

#define MAX(x, y) ((x >= y) ? x : y)
#define MIN(x, y) ((x <= y) ? x : y)

#endif
