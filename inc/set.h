#ifndef IMPORT_SET
#define IMPORT_SET

typedef struct{
  linkedList list;
}set;

/**
 * \fn set setCreate()
 * \brief Crée un set
 * \return Un set
 */
set setCreate();

/**
 * \fn boolean setIsEmpty(set set)
 * \brief Verifie si un set est vide
 * \param set Le set
 * \return True si le set est vide, false sinon
 */
boolean setIsEmpty(set set);

/**
 *
 *
 *
 *
 */
int setLength(set set);

boolean setContains(set set, void * object, int sizeOf);

collectionEntry setAdd(set * set, void * object, int sizeOf);

void setRemove(set * set, void * object, int sizeOf);

void setDestroy(set set);

#endif
