#ifndef IMPORT_HUNTER
#define IMPORT_HUNTER

#include "linkedList.h"
#include "boolean.h"
#include "terrain.h"

typedef struct{
  boolean isAI;
  int nbStunt;
  linkedList allSeen;
  int radius;
}hunter;

/**
 * \fn hunter createHunter(boolean isAI)
 * \brief Crée un hunter
 * \param isAi Est ce que le hunter est une IA
 * \return Un hunter
 */
hunter createHunter(boolean isAI);

position hunterPlay(hunter * hunt, terrain ter, int turn);

/**
 * \fn void hunterDestroy(hunter hunt)
 * \brief Détruit un hunter
 * \param hunt Le hunter
 */
void hunterDestroy(hunter hunt);
#endif
