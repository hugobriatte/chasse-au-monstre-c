#ifndef IMPORT_BOOLEAN
#define IMPORT_BOOLEAN
/**
 * \enum boolean
 * Enumération représentant le type boolean avec true et false
 */
typedef enum {false, true} boolean;
#endif
