#ifndef IMPORT_MONSTER
#define IMPORT_MONSTER
#include "position.h"
#include "direction.h"
#include "boolean.h"

/**
 * \struct monster monster.h
 * Structure définissant un monstre dans le jeu
 */
typedef struct{
  position pos; /*!< La position du monstre */
  boolean isFree; /*!< Est ce que le monstre peut bouger? */
  boolean isAI; /*!< Est ce que le monstre est une IA?*/
}monster;

/**
 * \fn monster createMonsterPos(position pos, boolean isAI)
 * \brief Crée un monstre a une position donnée
 * \param pos La position
 * \param isAI Est ce que le monstre est une intelligence artificielle?
 * \return Le monstre correspondant
 */
monster createMonsterPos(position pos, boolean isAI);

/**
 * \fn monster createMonsterXY(int x, int y, boolean isAI)
 * \brief Crée un monstre a des coordonnées donnée
 * \param x La coordonnée x
 * \param y La coordonnée y
 * \param isAI Est ce que le monstre est une intelligence artificielle?
 * \return Le monstre correspondant
 */
monster createMonsterXY(int x, int y, boolean isAI);

#endif
