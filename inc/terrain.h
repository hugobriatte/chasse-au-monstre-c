#ifndef IMPORT_TERRAIN
#define IMPORT_TERRAIN

#include "cell.h"
#include "boolean.h"
#include "map.h"
#include "position.h"
#include "collections.h"
#include "arrayList.h"
#include "monster.h"
#include "ioUtils.h"

/**
 * \struct terrain terrain.h
 * Structure représentant le terrain de jeu avec le nombre de cellules sur l'axe X, Y et le tableau a 2 dimension des cellules
 */
typedef struct{
  int nbCellX; /*!< Le nombre de cellules sur l'axe X */
  int nbCellY; /*!< Le nombre de cellules sur l'axe Y */
  cell ** cells; /*!< Le tableau de cellule */
  monster monstre;
}terrain;

/**
 * \fn terrain terrainCreateWith(int nbCellX, int nbCellY, boolean isThereWall, boolean isThereViewPoint, boolean isThereTp)
 * \brief Fonction qui retourne un terrain
 * \param nbCellX Le nombre de cellules sur l'axe X
 * \param nbCellY Le nombre de cellules sur l'axe Y
 * \param isThereWall Est ce qu'il y'a des murs?
 * \param isThereViewPoint Est ce qu'il y'a des pointes de vue?
 * \param isThereTp Est ce qu'il y'a des cellules de tp?
 * \return Un terrain
 */
terrain terrainCreateWith(int nbCellX, int nbCellY, boolean isThereWall, boolean isThereViewPoint, boolean isThereTp, boolean monsterAI);

/**
 * \fn terrain terrainCreate(int nbCellX, int nbCellY)
 * \brief Fonction qui retourne un terrain de Walkable_Cell simple (=terrainCreateWith(nbCellX, nbCellY, false, false, false))
 * \param nbCellX Le nombre de cellules sur l'axe X
 * \param nbCellY Le nombre de cellules sur l'axe Y
 * \return Un terrain généré
 */
#define terrainCreate(nbCellX, nbCellY, monsterAI) terrainCreateWith(nbCellX, nbCellY, false, false, false, monsterAI)

/**
 * \fn cell * getCellXY(terrain ter, int x, int y)
 * \brief Fonction qui retourne une cellule selon des coordonnées X et Y
 * \param ter Le terrain
 * \param x La coordonnée x
 * \param y la coordonnée y
 * \return Le pointeur sur la cellule
 */
cell * getCellXY(terrain ter, int x, int y);

/**
 * \fn cell * getCellPos(terrain ter, position pos)
 * \brief Fonction qui retourne une cellule selon une position
 * \param ter Le terrain
 * \param pos La position
 * \return Le pointeur sur la cellule
 */
cell * getCellPos(terrain ter, position pos);


/**
 * \fn void placePiegeXY(terrain ter, int x, int y)
 * \brief Place un piège sur une case selon des coordonnées x et y
 * \param ter Le terrain
 * \param x La coordonnée x
 * \param y La coordonnée y
 */
void placePiegeXY(terrain ter, int x, int y);

/**
 * \fn void placePiegePos(terrain ter, position pos)
 * \brief Place un piège sur une case selon une position
 * \param ter Le terrain
 * \param pos La position
 */
void placePiegePos(terrain ter, position pos);


/**
 * \fn boolean isCompletelyVisited(terrain ter)
 * \brief Retourne si le terrain a bien été visité entierement
 * \param ter le terrain
 * \return true si le terrain a été complétement visité, false sinon
 */
boolean isCompletelyVisited(terrain ter);


/**
 * \fn boolean isPosValid(terrain ter, position pos)
 * \brief Verifie si une position donnée est correct (est sur le terrain)
 * \param ter Le terrain
 * \param pos La position
 * \return true si la position existe sur le terrain, false sinon
 */
boolean isPosValid(terrain ter, position pos);

/**
 * \fn boolean isXYValid(terrain ter, int x, int y)
 * \brief Verifie si une coordonnée est correct (est sur le terrain)
 * \param ter Le terrain
 * \param x La coordonnée sur l'axe x
 * \param y La coordonnée sur l'axe y
 * \return true si la coordonnée existe sur le terrain, false sinon
 */
boolean isXYValid(terrain ter, int x, int y);


/**
 * \fn map getAllNearPos(terrain ter, position pos)
 * \brief Retourne une map associant a chaque direction la position autout d'un point si elle est valide
 * \param ter Le terrain
 * \param pos La position
 * \return Une map associant a chaque direction la position autout d'un point si elle est valide
 */
map getAllNearPos(terrain ter, position pos);

/**
 * \fn map getAllNearXY(terrain ter, int x, int y)
 * \brief Retourne une map associant a chaque direction la position autout d'un point si elle est valide
 * \param ter Le terrain
 * \param x La coordonnée x
 * \param y La coordonnée y
 * \return Une map associant a chaque direction la position autout d'un point si elle est valide
 */
map getAllNearXY(terrain ter, int x, int y);


/**
 * \fn map getAllNearWalkxablePos(terrain ter, position pos)
 * \brief Retourne une map associant a chaque direction la position autour d'un point si elle pointe une cellule marchable
 * \param ter Le terrain
 * \param pos La position
 * \return Une map associant a chaque direction la position autour d'un point si elle pointe une cellule marchable
 */
map getAllNearWalkablePos(terrain ter, position pos);

/**
 * \fn map getAllNearWalkableXY(terrain ter, int x, int y)
 * \brief Retourne une map associant a chaque direction la position autour d'un point si elle pointe une cellule marchable
 * \param ter Le terrain
 * \param x La coordonnée x
 * \param y La coordonnée y
 * \return Une map associant a chaque direction la position autour d'un point si elle pointe une cellule marchable
 */
map getAllNearWalkableXY(terrain ter, int x, int y);


/**
 * \fn map getAllNearWalkableNotSeenPos(terrain ter, position pos)
 * \brief Retourne une map associant a chaque direction la position autour d'un point si elle pointe une cellule marchable pas encore vu
 * \param ter Le terrain
 * \param pos La position
 * \return Une map associant a chaque direction la position autour d'un point si elle pointe une cellule marchable pas encore vu
 */
map getAllNearWalkableNotSeenPos(terrain ter, position pos);

/**
 * \fn map getAllNearWalkableNotSeenXY(terrain ter, int x, int y)
 * \brief Retourne une map associant a chaque direction la coordonnée autour d'un point si elle pointe une cellule marchable pas encore vu
 * \param ter Le terrain
 * \param x La coordonnée x
 * \param y La coordonnée y
 * \return Une map associant a chaque direction la position autour d'un point si elle pointe une cellule marchable pas encore vu
 */
map getAllNearWalkableNotSeenXY(terrain ter, int x, int y);

/**
 * \fn arrayList getPath(terrain ter, int nbCell, position cellPos)
 * \brief Retourne une liste contenant dans l'ordre une suite de position formant un chemin non encore exploré par le monstre
 * \param ter Le terrain
 * \param nbCell Le nombre de cellules maximum (voulu) sur le chemin
 * \param cellPos La position de départ
 * \return Une liste contenant dans l'ordre une suite de position formant un chemin non encore exploré par le monstre
 */
linkedList getPath(terrain ter, int nbCell, position cellPos);

/**
 * \fn void terrainDestroy(terrain ter)
 * \brief Détruit un terrain
 * \param ter Le terrain
 */
void terrainDestroy(terrain ter);

/**
 * \fn void drawBoardHunter(terrain ter, int nbChar)
 * \brief Affiche le terrain pour le chasseur
 * \param ter Le terrain
 * \param nbChar Le nombre de caractère par case
 */
void drawBoardHunter(terrain ter, int nbChar);

/**
 * \fn void drawBoardMonster(terrain ter, int nbChar)
 * \brief Affiche le terrain pour le monstre
 * \param ter Le terrain
 * \param nbChar Le nombre de caractère par case
 */
void drawBoardMonster(terrain ter, int nbChar);

/**
 * \fn void drawBoardMonsterComplete(terrain ter, int nbChar)
 * \brief Affiche le terrain de manière complète pour le monstre
 * \param ter Le terrain
 * \param nbChar Le nombre de caractère par case
 */
void drawBoardMonsterComplete(terrain ter, int nbChar);

/**
 * \fn void monsterMoveToPos(terrain * ter, position pos)
 * \brief Déplace le monstre sur une position donnée
 * \param ter Le pointeur sur le terrain
 * \param pos La position
 */
void monsterMoveToPos(terrain * ter, position pos);

/**
 * \fn void monsterMoveToDir(terrain * ter, direction dir)
 * \brief Déplace le monstre vers une direction donnée
 * \param ter Le pointeur sur le terrain
 * \param dir La direction
 */
void monsterMoveToDir(terrain * ter, direction dir);

/**
 * \fn void monsterMoveToXY(terrain * ter, int x, int y)
 * \brief Déplace le monstre vers une coordonnée donnée
 * \param x La coordonnée x
 * \param y La coordonnée y
 * \param ter Le pointeur sur le terrain
 */
void monsterMoveToXY(terrain * ter, int x, int y);

/**
 * \fn cell * monsterGetCell(terrain ter)
 * \brief Retourne la cellule du monstre
 * \param ter Le terrain
 * \return La cellule du monstre
 */
cell * monsterGetCell(terrain ter);

/**
 * \fn void monsterPlay(terrain * ter)
 * \brief Lance un tour du monstre
 * \param ter Un pointeur sur le terrain
 */
void monsterPlay(terrain * ter);

/**
 * \fn linkedList getAllWalkableCellTer(terrain ter)
 * \brief Retourne toutes les cellules marchables du terrain
 * \param ter Un terrain
 * \return Toutes ses cellules marchable sous la forme d'une linkedList
 */
arrayList getAllPosWalkableTer(terrain ter);

/**
 * \fn linkedList getAllNotSeenTer(terrain ter)
 * \brief Retourne touotes les cellules marchables et non vue du terrain
 * \param ter Un terrain
 * \return Toutes ses cellules marchables et non vue
 */
arrayList getAllPosNotSeenTer(terrain ter);

#define getAllAccesibleHunter(ter, pos, nbCell) getAllAccesibleAndRemoveHunter(ter, pos, nbCell, emptyLinkedList)

#define getAllAccesibleMonster(ter, pos, nbCell) getAllAccesibleAndRemoveMonster(ter, pos, nbCell, emptyLinkedList)

arrayList getAllAccesibleAndRemoveHunter(terrain ter, position pos, int nbCell, linkedList list);

arrayList getAllAccesibleAndRemoveMonster(terrain ter, position pos, int nbCell, linkedList list);
#endif
