#ifndef IMPORT_COLLECTION_ENTRY
#define IMPORT_COLLECTION_ENTRY
#include <stdlib.h>
#include "boolean.h"

/**
 * \struct collectionEntry list.h
 * Structure représentant une entrée d'une collection
 */
typedef struct {
  void * pointer; /*!< Un pointeur sur la valeur stocké */
  int size; /*!< La taille de la valeur stocké */
}collectionEntry;

/** Une entrée null dans le tableau */
static const collectionEntry emptyCollectionEntry = {NULL, 0};

#endif
