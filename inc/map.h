#ifndef IMPORT_MAP
#define IMPORT_MAP
#include "boolean.h"
#include "linkedList.h"
#include "arrayList.h"
/**
 * \struct map
 * Map associant a chaque valeur d'une énumération (ou d'un nombre, mais le maximum doit être précisé) un pointeur vers une valeur quelquoncque. Pour une utilisation sans problème, il est conseillé de stocker des clés/valeur de même taille
 */
typedef struct{
  linkedList keyList; /*!< La liste contenant les clés */
  arrayList valueList; /*!< La liste contenant les valeurs */
} map;

/**
 * \fn map mapCreate()
 * \brief Crée une map
 * \return Une map
 */
map mapCreate();

/**
 * \fn boolean mapPlaced(map map, void * key, int sizeOf)
 * \brief Indique si une valeur a été renseigné pour la valeur donnée
 * \param map La map
 * \param key La clé
 * \param sizeOf La teille de la clé
 * \return true si une valeur a été placé, false sinon
 */
boolean mapPlaced(map map, void * key, int sizeOf);

/**
 * \fn void mapPut(map * map, void * key, int sizeOfKey, void * value, int sizeOfValue)
 * \brief Place une valeur avec une clé donnée dans la map
 * \param map Un pointeur sur la map
 * \param key La clé
 * \param sizeOfKey La clé
 * \param value Le pointeur sur la valeur a placer
 * \param sizeOfValue La taille de la valeur
 */
void mapPut(map * map, void * key, int sizeOfKey, void * value, int sizeOfValue);

/**
 * \fn void mapRemove(map * map, void * key, int sizeOfKey)
 * \brief Enleve une valeur de la map
 * \param map Un pointeur sur la map
 * \param key La clé
 * \param sizeOfKey La taille de la clé
 */
void mapRemove(map * map, void * key, int sizeOfKey);

/**
 * \fn collectionEntry mapGet(map map, void * key, int sizeOfKey)
 * \brief Retourne un pointeur sur la valeur stocké a un endroit donnée
 * \param map La map
 * \param key La clé
 * \param sizeOfKey La taille de la clé
 * \return Un objet contenant le pointeur sur la valeur stocké avec cet clé et la taille de la valeur stocké
 */
collectionEntry mapGet(map map, void * key, int sizeOfKey);

/**
 * \fn void enumMapDestroy(enumMap map)
 * \brief Détruit une map
 * \param map La map
 */
void mapDestroy(map * map);

#define FOREACHKEY(voidP, map) FOREACHLINKEDLIST(voidP, map.keyList)
#define FOREACHVALUE(voidP, map) FOREACHARRAYLIST(voidP, map.valueList)
#endif
