#ifndef IMPORT_COLLECTIONS
#define IMPORT_COLLECTIONS
#include "linkedList.h"
#include "arrayList.h"
#include "map.h"
/**
 * \struct collection collections.h
 * Structure contenant une liste
 */
typedef struct{
  char type; /*!< Char représentant le type */
  union{
    arrayList * array; /*!< ArrayList */
    linkedList * linked; /*!< LinkedList */
  }list; /*<! Union contenant suit une arrayList ou bien une linkedList */
}collection;

/**
 * \fn void collectionAddAll(collection dest, collection src)
 * \brief Ajoute tout les objets d'une collection dans une autre
 * \param dest La destination
 * \param src La source
 */
void collectionAddAll(collection dest, collection src);

/**
 * \fn void collectionRemoveAll(collection removeIn, collection toRemove)
 * \brief Enleve dans une collection tout les objets d'une autre collection
 * \param removeIn La collection dans laquelle enlever
 * \param toRemove La collection contenant des objets a enlever
 */
void collectionRemoveAll(collection removeIn, collection toRemove);

/**
 * \fn collection linkedListAsCollection(linkedList * list)
 * \brief Retourne la collection associé a une linkedList
 * \param list Le pointeur sur la linkedList
 * \return La collection associé
 */
collection linkedListAsCollection(linkedList * list);

/**
 * \fn collection arrayListAsCollection(arrayList * list)
 * \brief Retourne la collection associé a une arrayList
 * \param list Le pointeur sur l'arrayList
 * \return La collection associé
 */
collection arrayListAsCollection(arrayList * list);
#endif
