#ifndef IMPORT_CELL
#define IMPORT_CELL
#include "boolean.h"
/**
 * \enum cellType
 * \brief Enumération représentant les differentes cellules possibles
 */
typedef enum {WALL, WALKABLE_CELL, VIEW_POINT_CELL, STUNT_TRAP, TP_CELL} cellType;
/**
 * \struct cell
 * Structure représentant une cellule
 */
typedef struct {
  cellType typeCell; /*!< Le type de la cellule */
  int turn; /*!< Le tour durant lequel le monstre a marché dessus (-1 si pas encore marché dessus) */
}cell;

/**
 * \fn cellType getNextCellType(boolean isThereWall, boolean isThereViewPoint, boolean isThereTp)
 * \brief Retourne une type de cellule selon les paramètres données
 * \param isThereWall Est ce que ca peut être un mur?
 * \param isThereViewPoint Est ce que ca peut être un viewPoint?
 * \param isThereTp Est ce que ca peut être un Tp?
 * \return Un cellType correspondant
 */
cellType getNextCellType(boolean isThereWall, boolean isThereViewPoint, boolean isThereTp);

/**
 * \fn cell createCellType(cellType type)
 * \brief Crée une cellule d'un type donnée non visité
 * \param type Le type de la cellule
 * \return Une cellule d'un type donnée non visité
 */
cell createCellType(cellType type);

/**
 * \fn cell createCell(cellType type, int turn)
 * \brief Crée une cellule d'un type donnée et avec un tour donnée
 * \param type Le type de la cellule
 * \param turn Le tour donnée
 * \return Une cellule d'un type donnée et avec un tour donnée
 */
cell createCell(cellType type, int turn);

/**
 * \fn char * cellCharForMonster(cell c, int nbChar)
 * \brief Fonction qui retourne un String représentant la cellule en un certain nombre de cases pour le monstre
 * \param c La cellule
 * \param nbChar le nombre de caractère nécéssaire
 * \return Un String représentant la cellule en un certain nombre de cases pour le monstre
 */
char * cellCharForMonster(cell cell, int nbChar);

/**
 * \fn char * cellCharForMonsterComplete(cell c, int nbChar)
 * \brief Fonction qui retourne un String représentant la cellule en un certain nombre de cases pour le monstre de manière compléte
 * \param c La cellule
 * \param nbChar le nombre de caractère nécéssaire
 * \return Un String représentant la cellule en un certain nombre de cases pour le monstre de manière compléte
 */
char * cellCharForMonsterComplete(cell cell, int nbChar);

/**
 * \fn char * cellCharForHunter(cell c, int nbChar)
 * \brief Fonction qui retourne un String représentant la cellule en un certain nombre de cases pour le chasseur
 * \param c La cellule
 * \param nbChar le nombre de caractère nécéssaire
 * \return Un String représentant la cellule en un certain nombre de cases pour le chasseur
 */
char * cellCharForHunter(cell cell, int nbChar);

/**
 * \fn boolean hasBeenVisited(cell c)
 * \brief Fonction qui indique si une case a déjà été visité
 * \param c La cellule
 * \return true si elle a été visité, false sinon
 */
boolean hasBeenVisited(cell cell);
#endif
