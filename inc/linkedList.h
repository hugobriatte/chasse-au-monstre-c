#ifndef IMPORT_LINKED_LIST
#define IMPORT_LINKED_LIST
#include <stddef.h>
#include "collectionEntry.h"
#include "boolean.h"

/**
 * \struct linkedListEntry
 * Structure définissant une entrée dans une linkedList
 */
struct linkedListEntry{
  collectionEntry entry; /*!< Les données contenu dans l'entrée */
  struct linkedListEntry * next; /*!< Un pointeur sur l'entrée suivante */
};
typedef struct linkedListEntry * linkedList;

static const struct linkedListEntry emptyLinkedListEntry = {{NULL, 0}, NULL};

/** Une linkedList vide */
static const linkedList emptyLinkedList = NULL;

/**
 * \fn int linkedListLength(linkedList list)
 * \brief Retourne la longeur de la liste
 * \param list La liste
 * \return La longeur de la liste
 */
int linkedListLength(linkedList list);

/**
 * \fn boolean linkedListIsEmpty(linkedList list)
 * \brief Indique si la liste est vide ou non
 * \param list La liste
 * \return true si la liste est vide, false sinon
 */
boolean linkedListIsEmpty(linkedList list);

/**
 * \fn collectionEntry linkedListHead(linkedList list)
 * \brief Retourne la donnée contenu dans la tête de la list
 * \param list La liste
 * \return La donnée contenu dans la tête de la list
 */
collectionEntry linkedListHead(linkedList list);

/**
 * \fn linkedList linkedListQueue(linkedList list)
 * \brief Retourne le reste de la liste
 * \param list La liste
 * \return Le reste de la liste
 */
linkedList linkedListQueue(linkedList list);

/**
 * \fn void linkedListAddObjIdx(linkedList * list, int idx, void * object, int sizeOf)
 * \brief Ajoute un objet a un index donnée
 * \param list Un pointeur sur la liste
 * \param idx L'index ou ajouter l'objet
 * \param object L'objet a ajouter
 * \param sizeOf la taille de l'objet
 */
void linkedListAddObjIdx(linkedList * list, int idx, void * object, int sizeOf);

/**
 * \fn void linkedListAddObj(linkedList * list, void * object, int sizeOf)
 * \brief Ajoute un objet au début de la liste
 * \param list Le pointeur sur la liste
 * \param object Un pointeur sur l'objet a ajouter
 * \param sizeOf La taille de l'objet a ajouter
 */
void linkedListAddObj(linkedList * list, void * object, int sizeOf);

/**
 * \fn collectionEntry linkedListGetIdx(linkedList list, int index)
 * \brief Retourne la donnée stocké a un certain index
 * \param list La liste
 * \param index L'index ou récupérer la donnée
 * \return La donnée stocké a cette donnée (contenant NULL et 0 si vide)
 */
collectionEntry linkedListGetIdx(linkedList list, int index);

/**
 * \fn void linkedListRemoveObj(linkedList * list, void * object, int sizeOf)
 * \brief Enlève un objet de la liste
 * \param list Le pointeur sur la liste
 * \param object Un pointeur sur l'objet a ajouter
 * \param sizeOf La taille de l'objet
 */
void linkedListRemoveObj(linkedList * list, void * object, int sizeOf);

/**
 * \fn void linkedListRemoveIdx(linkedList * list, int index)
 * \brief Enlève l'objet a un certain index
 * \param list Le pointeur sur la liste
 * \param index L'index de l'objet a enlever
 */
void linkedListRemoveIdx(linkedList * list, int index);

/**
 * \fn int linkedListIndexOf(linkedList list, void * object, int sizeOf)
 * \brief Retourne l'index d'un objet dans la liste
 * \param list La liste
 * \param object Le pointeur sur l'objet
 * \param sizeof La taille de l'objet
 * \return L'index de l'objet (-1 si il n'est pas présent)
 */
int linkedListIndexOf(linkedList list, void * object, int sizeOf);

/**
 * \fn void linkedListDestroy(linkedList list)
 * \brief Détruit une liste
 * \param list La liste
 */
void linkedListDestroy(linkedList list);

/** Définit une boucle foreach avec collectionEP un pointeur sur une collectionEntry et list la linkedList*/
#define FOREACHLINKEDLIST(collectionEP, list) for(collectionEP = (void *) list; collectionEP != NULL; collectionEP = (void *)(((struct linkedListEntry *) collectionEP)->next))
#endif
