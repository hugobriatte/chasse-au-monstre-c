#ifndef IMPORT_LIST
#define IMPORT_LIST

#include "boolean.h"
#include "collectionEntry.h"
#include "randUtil.h"
#include <stdlib.h>
#include <stdarg.h>

/**
 * \struct list list.h
 * Structure représentant une liste (implémenté tel une ArrayList) qui peut contenir des pointeurs quelquoncques
 */
typedef struct {
  int maxSize; /*!< La taille maximale avant la recopie du tableau */ 
  int size; /*!< La taille actuel de la liste */
  collectionEntry * tab; /*!< Le tableau contenant les valeurs */
}arrayList;

/**
 * \fn list listCreateWithSize(int defSize)
 * \brief Crée une liste avec une taille maximale par default
 * \param defSize La taille maximale par defaut
 * \return Une liste avec pour taille maximale par default defSize
 */
#define arrayListCreate() arrayListCreateWithSize(4)

/**
 * \fn list listCreate()
 * \brief Crée une liste
 * \return Une liste
 */
arrayList arrayListCreateWithSize(int defSize);

/**
 * \fn list listCreateWithDefault(void * toCopy, int sizeOf, int nbItems)
 * \brief Crée une liste bourré avec un certain élément
 * \param toCopy Un pointeur sur l'élément a copier
 * \param sizeOf La taille de l'élément a copier
 */
arrayList arrayListCreateWithDefault(void * toCopy, int sizeOf, int nbItems);

/**
 * \fn boolean listIsEmpty(list list)
 * \brief Verifie si une liste est vide ou non
 * \param list la liste
 * \return true si elle est vide, false sinon
 */
boolean arrayListIsEmpty(arrayList list);

/**
 * \fn int listLength(list list)
 * \brief Retourne la longeur d'une liste
 * \param list la liste
 * \return La longeur de la liste
 */
int arrayListLength(arrayList list);

/**
 * \fn void * listGet(list list, int i)
 * \brief Retourne le pointeur sur la cellule a un index donnée
 * \param list La liste
 * \param i L'index
 * \return Un objet contenant un pointeur sur l'objet contenu et la taille de l'objet
 */
collectionEntry arrayListGet(arrayList list, int i);

/**
 * \fn void listRemoveIdx(list * list, int idx)
 * \brief Enleve un objet de la liste a un index donnée
 * \param list Le pointeur sur la liste
 * \param idx L'index de objet a enlever
 */
void arrayListRemoveIdx(arrayList * list, int idx);

/**
 * \fn void listRemoveObj(list * list, void * object, int sizeOf)
 * \brief Enleve un objet de la liste
 * \param list Le pointeur sur la liste
 * \param object Le pointeur sur l'objet
 * \param sizeOf La taille de l'objet a contenir
 */
void arrayListRemoveObj(arrayList * list, void * object, int sizeOf);

/**
 * \fn int listIndexOf(list list, void * object, int sizeOf)
 * \brief Retourne l'index ou se situe un objet dans la liste
 * \param list La liste
 * \param object Le pointeur sur l'objet a ajouter
 * \param sizeOf La taille de l'objet a contenir
 */
int arrayListIndexOf(arrayList list, void * object, int sizeOf);

/**
 * \fn void listSetObj(list list, int i, void * object)
 * \brief Remplace l'objet a un index donnée par un autre
 * \param list La liste
 * \param i L'index ou placer le pointeur
 * \param object Le ponteur sur l'objet a placer
 * \param sizeOf La taille de l'objet a contenir
 */
void arrayListSetObj(arrayList list, int idx, void * object, int sizeOf);

/**
 * \fn void listAddObjIdx(list * list, int i, void * object)
 * \brief Ajoute un objet dans la liste a un index donnée
 * \param list Le pointeur sur la liste
 * \param i L'index ou placer le nouveau object
 * \param object Le pointeur sur l'objet a ajouter
 * \param sizeOf La taille de l'objet a contenir
 */
void arrayListAddObjIdx(arrayList * list, int idx, void * object, int sizeOf);

/**
 * \fn void listAddObj(list * list, void * object)
 * \brief Ajoute un objet dans la liste a la fin
 * \param list Le pointeur sur la liste
 * \param object Le pointeur sur l'objet a ajouter
 * \param sizeOf La taille de l'objet a ajouter
 */
void arrayListAddObj(arrayList * list, void * object, int sizeOf);

/**
 * \fn void listDestroy(list * list)
 * \brief Supprime totalement une liste
 * \param list Le pointeur sur la liste a supprimer
 */
void arrayListDestroy(arrayList * list);

/**
 * \fn boolean listIsValidIdx(list list, int i)
 * \brief Verifie si l'index donnée appartient a la liste
 * \param list La liste
 * \param i L'index
 */
boolean arrayListIsValidIdx(arrayList list, int i);

/**
 * \fn void arrayListSort(arrayList list, int (*sortFunction)(void *, void *, void *), void * externalArgs)
 * \brief Trie une arrayList grace a une fonction donnée
 * \param list La liste a trie
 * \param sortFunction Une fonction comparant deux valeurs et retournant -1, 0 ou 1 si l'objet pointé par le 1er pointeur est plus petit, égale ou plus grand que le second objet pointé par le second pointeur. Il accepte un troisième argument pouvant être n'importe quoi
 * \param externalArgs L'argument exterieur a donner
 */
void arrayListSort(arrayList list, int (*sortFunction)(void *, void *, void *), void * externalArgs);

/** Macro pour iterer sur chaque item d'une list (collectionE : pointeur sur collectionEntry; list : l'arrayList) */
#define FOREACHARRAYLIST(voidP, list) for(voidP = (void *) list.tab; ((collectionEntry *) voidP) - list.tab < list.size; voidP = (void *) (((collectionEntry *) voidP) + 1))

#define arrayListGetRandom(list) arrayListGet(list, randMax(arrayListLength(list)))
#endif
