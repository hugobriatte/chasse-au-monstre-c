#ifndef IMPORT_RANDUTIL
#define IMPORT_RANDUTIL

/**
 * \fn int randMinMax(int min, int max)
 * \brief Retourne une valeur entre min (inclusif) et max (exclusif)
 * \param min La valeur minimale (inclusif)
 * \param max La valeur maximale (exclusif)
 * \return Une valeur dans [min;max[
 */
int randMinMax(int min, int max);

#define randMax(max) randMinMax(0, max)

#endif
