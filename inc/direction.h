#ifndef IMPORT_DIRECTION
#define IMPORT_DIRECTION
#include <stdio.h>
/**
 * \enum direction
 * Enumération représentations les differentes directions
 */
typedef enum{
  LEFT_UP, LEFT, LEFT_DOWN, UP, NONE, DOWN, RIGHT_UP, RIGHT, RIGHT_DOWN
} direction;

/**
 * \fn int getDirX(direction d)
 * \brief Fonction qui retourne la coordonnée X associé a la direction
 * \param d La direction
 * \return La coordonnée X associé a cette direction
 */
int getDirX(direction d);

/**
 * \fn int getDirY(direction d)
 * \brief Fonction qui retourne la coordonnée Y associé a la direction
 * \param d La direction
 * \return La coordonnée Y associé a cette direction
 */
int getDirY(direction d);

/**
 * \fn direction readDirection()
 * \brief Fonction qui lit la direction indiqué dans stdin (=readDirectionOf(stdin))
 * \return La direction associé
 */
direction readDirection();

/**
 * \fn direction readDirectionOf(FILE * file)
 * \brief Fonction qui lit la direction dans le flux file
 * \param file Le flux
 * \return La direction associé
 */
direction readDirectionOf(FILE * file);
#endif
