#ifndef IMPORT_POSITION
#define IMPORT_POSITION
#include "direction.h"
#include "boolean.h"

/**
 * \struct position position.h
 * Structure contenant deux coordonnées : x et y. Il permet de décrire la position d'un objet dans l'espace de jeu
 */
typedef struct {
  int x; /*!< La coordonnée sur X */
  int y; /*!< La coordonnée sur Y */
} position;

/** Position non-mutable */
typedef const position fixedPosition;

/**
 * \fn position createPosition(int x, int y)
 * \brief Crée une position
 * \param x La coordonnée X
 * \param y La coordonnée Y
 * \return La position
 */
position createPosition(int x, int y);

/**
 * \fn position getAddDir(position pos, direction dir)
 * \brief Fonction qui retourne la position situé a une certaine direction
 * \param pos La position
 * \param dir La direction
 * \return La position situé a une certaine direction
 */
position getAddDir(position pos, direction dir);

/**
 * \fn position getAddPos(position posA, position posB)
 * \brief Fonction qui retourne l'addition des deux positions
 * \param posA La 1ere position
 * \param posB La 2eme position
 * \return La position par addition entre deux matrice
 */
position getAddPos(position posA, position posB);

/**
 * \fn position getAddXY(position pos, int x, int y)
 * \brief Fonction qui retourne une position auquel on ajoute des coordonnées x et y
 * \param pos La position
 * \param x La coordonnée x
 * \param y La coordonnée y
 * \return La position auquel on ajoute les coordonnées x et y
 */
position getAddXY(position pos, int x, int y);

/**
 * \fn position getPosOf(direction d)
 * \brief Fonction qui retourne la position associé a une direction
 * \param d La direction
 * \return La position associé a la direction
 */
position getPosOf(direction d);

/**
 * \fn boolean positionEquals(position d1, position d2)
 * \brief Fonction qui verifie si deux positions sont égales
 * \param d1 La première direction
 * \param d2 La seconde direction
 * \return true si elles sont égales, false sinon
 */
boolean positionEquals(position d1, position d2);

/**
 * \fn void movePosToDir(position * pos, direction direction)
 * \brief Processus pour déplacer une position vers une direction donnée
 * \param pos Le pointeur vers la position
 * \param direction La direction
 */
void movePosToDir(position * pos, direction direction);

/**
 * \fn void translateWithPos(position * posOrigin, position vector)
 * \brief Processus pour déplacer une position par rapport a une position (Translation)
 * \param pos Le pointeur vers la position
 * \param vector Le vecteur a utiliser
 */
void translateWithPos(position * posOrigin, position vector);

/**
 * \fn void translateWithInt(position * pos, int x, int y)
 * \brief Processus pour réaliser une translation avec des coordonnées donnée
 * \param pos Le pointeur vers la position
 * \param x La coordonnée x
 * \param y La coordonnée y
 */
void translateWithInt(position * pos, int x, int y); 
#endif
